declare -a UOS
demanaUO(){
        UOS=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=dcObject)" | grep dn: | cut -d " " -f 2) $(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=OrganizationalUnit)" | grep dn: | cut -d " " -f 2 ) $(cat uo.ldif | grep dn: | cut -d " " -f 2 )) 2> /dev/null
        contador=0
        echo "Selecciona la UO on es troba"
        for i in "${UOS[@]}"
        do
            echo "$contador: $i"
            ((contador=$contador+1))
        done
}
demanaUO
read llocUO
#exemple d'ús a users
echo "dn: cn="$usrname" "$apellido","${UOS[$llocUO]} >> usr.ldif

declare -a USR
declare -a UserID
demanaUSR(){
    contador=0
    USR=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixAccount)" | grep dn: | cut -d " " -f 2,3 | sed 's/ /\¬/'))
    echo "Selecciona l'usuari:"
    for i in "${USR[@]}"
    do
        echo "$contador: $(echo $i | sed 's/\¬/ /')"
        (( contador=$contador+1 ))
    done
    UserID=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixAccount)" | grep uidNumber: | cut -d " " -f 2 ))
}

declare -a GRP
declare -a GUID
demanaGRP(){
    contador=0
        GRP=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixGroup)" | grep dn: | cut -d " " -f 2) )
        echo "Selecciona l'usuari:"
        for i in "${GRP[@]}"
        do
            echo "$contador: $i"
            (( contador=$contador+1 ))
        done

    GUID=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixGroup)" | grep gidNumber: | cut -d " " -f 2) )
}


        demanaUSR
        read usuari
        echo "L'usuari escollit és: "
                echo $(echo "${USR[$usuari]}" | sed 's/\¬/ /')
        echo "El seu UID és: " ${UserID[$usuari]}
                demanaGRP
                read grp
                echo "El grup escollit és: "
                echo "${GRP[$grp]}"
        echo "El seu GUID és:" ${GUID[$grp]}
