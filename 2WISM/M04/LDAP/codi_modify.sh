#!/bin/bash
correctUSR="SI"
declare -a USR
declare -a UserID
demanaUSR(){
    contador=0
    USR=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixAccount)" | grep dn: | cut -d " " -f 2,3 | sed 's/ /\¬/'))
    echo "Selecciona l'usuari:"
    for i in "${USR[@]}"
    do
        echo "$contador: $(echo $i | sed 's/\¬/ /')"
        (( contador=$contador+1 ))
    done
    UserID=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixAccount)" | grep uid: | cut -d " " -f 2 ))
}

declare -a GRP
declare -a GUID
demanaGRP(){
    contador=0
        GRP=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixGroup)" | grep dn: | cut -d " " -f 2) )
        echo "Selecciona el grup:"
        for i in "${GRP[@]}"
        do
            echo "$contador: $i"
            (( contador=$contador+1 ))
        done

    GUID=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixGroup)" | grep gidNumber: | cut -d " " -f 2) )
}

while :
do
    echo -e  "Selecciona l'opció:\n\t1) modificar grup d'usuari.\n\t2) afegir usuari a un grup.\n\t3) esborrar un usuari.\n"
    read opcio
case $opcio in
    1)
    echo "El programa el guiarà per tal de modificar el grup de l'usuari"
    demanaUSR
    read usuari
    demanaGRP
    read grup

    #modificar atributs
    echo "L'usuari es modificarà de la següent manera:"
    echo "dn: $(echo "${USR[$usuari]}" | sed 's/\¬/ /g')"
    echo "changetype: modify"
    echo "replace: uidNumber"
    echo "uidNumber: "${GUID[$grup]}
    read -p "És correcte? Si/No " correctUSR
                check=${correctUSR^^}
                if [ $check == "SI" ]
                then
            echo "dn: $(echo "${USR[$usuari]}" | sed 's/\¬/ /g')" >> modifica.ldif
                echo "changetype: modify" >> modifica.ldif
                echo "replace: gidNumber" >> modifica.ldif
                echo "gidNumber: "${GUID[$grup]} >> modifica.ldif
        fi
    ldapmodify -x -D "cn=admin,dc=edt,dc=org" -w manel -f modifica.ldif
    echo > modifica.ldif
    ;;
    2)
    echo "El programa el guiarà per tal d'afegir usuaris a grups:"
    demanaUSR
    read usuari
    demanaGRP
    read grup

    #afegir atributs
    echo "L'usuari es modificarà de la següent manera:"
    echo "dn: ${GRP[$grup]}"
    echo changetype: modify
    echo add: memberUid
    echo memberUid: $(echo "${UserID[$usuari]}" | sed 's/\¬/ /g')
    read -p "És correcte? Si/No " correctUSR
                check=${correctUSR^^}
                if [ $check == "SI" ]
                then
                echo "dn: ${GRP[$grup]}" >> modifica.ldif
                echo changetype: modify >> modifica.ldif
                echo add: memberUid >> modifica.ldif
                echo memberUid: $(echo "${UserID[$usuari]}" | sed 's/\¬/ /g') >> modifica.ldif
        fi
        ldapmodify -x -D "cn=admin,dc=edt,dc=org" -w manel -f modifica.ldif
        echo > modifica.ldif
    ;;
    3)
    demanaUSR
        read usuari
    echo "El següent usuari s'esborrarà:"
    echo "dn: $(echo "${USR[$usuari]}" | sed 's/\¬/ /g')"
    echo "changetype: delete"

    read -p "És correcte? Si/No " correctUSR
                check=${correctUSR^^}
                if [ $check == "SI" ]
                then
            echo dn: $(echo "${USR[$usuari]}" | sed 's/\¬/ /g') >> modifica.ldif
                echo "changetype: delete" >> modifica.ldif
                fi
        ldapmodify -x -D "cn=admin,dc=edt,dc=org" -w manel -f modifica.ldif
        echo > modifica.ldif

    ;;
    *)
    echo "Sel·lecciona una opció entre 1 i 3"
    ;;
esac
done
