#!/bin/bash
correctUSR="SI"
declare -a USR
declare -a UserID
demanaUSR(){
    contador=0
    USR=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixAccount)" | grep dn: | cut -d " " -f 2,3 | sed 's/ /\¬/'))
    echo "Selecciona l'usuari:"
    for i in "${USR[@]}"
    do
        echo "$contador: $(echo $i | sed 's/\¬/ /')"
        (( contador=$contador+1 ))
    done
    UserID=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixAccount)" | grep uid: | cut -d " " -f 2 ))
}

declare -a GRP
declare -a GUID
demanaGRP(){
    contador=0
        GRP=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixGroup)" | grep dn: | cut -d " " -f 2) )
        echo "Selecciona el grup:"
        for i in "${GRP[@]}"
        do
            echo "$contador: $i"
            (( contador=$contador+1 ))
        done

    GUID=($(ldapsearch -x -LLL -b dc=edt,dc=org "(objectClass=posixGroup)" | grep gidNumber: | cut -d " " -f 2) )
}
