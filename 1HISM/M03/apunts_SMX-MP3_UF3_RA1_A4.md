# Crear fulls d’estil

Per poder gestionar documents de forma professional cal saber distingir entre
aquests tres aspectes de qualsevol document:

+ **Contingut** (responsabilitat de l’**autor** del text).
+ **Estructura** (responsabilitat de l’**editor** del text).
+ **Presentació** (responsabilitat del **grafista**).

El destinataris finals dels documents, els éssers humans que els han de llegir,
sovint no saben discriminar aquests tres aspectes, però nosaltres necessitem
aquest coneixement per poder gestionar documents complexos, compartits,
extensos, etc. Com veus en la llista anterior, cada un dels aspectes pot ser
responsabilitat de diferents perfils professionals: nosaltres haurem de fer els
papers dels tres professionals, _però no al mateix temps_!

## Contingut

El contingut d’un document és el seu text, creat pel seu _autor_: pots pensar
en el contingut com allò que podries comunicar per telèfon. Imagina’t que ets un
corresponsal de premsa en la segona guerra mundial, i que els teus reportatges
els has de llegir per telèfon des del front de guerra a alguna persona de la
redacció del diari: això és el contingut.

En aquest mòdul no treballarem la creació de continguts: el perfil professional
de qui pot redactar text, corregir-lo, si cal fer traduccions, etc. no és el
propi d’aquest cicle formatiu. En tot cas, aquest és un consell per quan hagis
de redactar continguts: oblida’t de la seva presentació, i deixa aquesta feina
pel final, una vegada hagis redactat el text.

Si no tenim continguts, i volem treballar l’estructura i la presentació d’un
document, podem usar text en [llatí][IPSUM] tal
com els grafistes acostumen a fer: fes tu el mateix a partir d’ara.

## Fulls d’estil

Tant per organitzar l’estructura dels documents, com la seva presentació,
disposem dels anomenats
[_fulls d’estil_](https://en.wikipedia.org/wiki/Style_sheet_(desktop_publishing)),
en ocasions agrupats en [plantilles](https://es.wikipedia.org/wiki/Plantilla),
tal com veurem en la [A5](./A5.md).

Podem imaginar cada un dels estils com llargues llistes de propietats, cada una
definint algun aspecte estructural o de presentació. Per tant, cada document
disposa en ser creat de la seva pròpia col·lecció d’estils, que poden ser
modificats lliurement i que seran completament independents dels estils que
tinguin altres documents.

Els estils individuals són creats per l’usuari i poden incloure una gran
varietat de propietats que dictaminen com es formata una porció de text
determinada, per exemple:

+ Tipus de lletra (serif, sans serif, etc.)
+ Negreta
+ Itàlica
+ Subratllat
+ Justificació (esquerra, dreta, centre, justificat)
+ Espai abans i després dels paràgrafs
+ Mida dels tabuladors i sagnat dels paràgrafs
+ Mida del tipus de lletra
+ Colors
+ Marges
+ Capçaleres i peus de pàgina
+ Vores
+ Superíndex o subíndex
+ Majúscules o minúscules
+ Guionat

En utilitzar els estils amb _LibreOffice Writer_ ens limitarem generalment a usar els
estils predefinits, i en tot cas modificarem la seva definició, però no crearem
_nous_ estils fora d’algun cas excepcional.

## Estructura

Seguint amb el nostre exemple del corresponsal de guerra, el text que a arribat
a la redacció serà editat per extreure títols i subtítols, decidir en quina
pàgina del diari ha d’anar, les columnes que ocuparà, etc. Tot això és feina de l’editor,
feina sobre la que l’autor no te control.

Per definir l’estructura del document l’editor disposa de dues eines:

+ Els _estils de pàgina_ per definir mida i orientació del full, marges, nombre
  de columnes, etc.
+ Els _estils de paràgraf_ que tenen assignat un _nivell d’esquema_ (1, 2, 3,
  etc.).  Els paràgrafs amb el nivell anomenat _Cos del text_ no tenen assignat
  cap nivell d’esquema i no participen per tant en la definició de l’estructura
  del document.

Els estils de paràgraf amb un nivell d’esquema assignat són molt
importants, i cal sempre començar a estructurar un document en base a aquests
estils predefinits: _Encapçalament 1_, _Encapçalament 2_, etc. Aquests estils
sempre s’han d’utilitzar de forma **ordenada**, és a dir, que si imaginem d’un
document tan sols els paràgrafs que tenen algun d’aquests estils, aquests
cal que apareixin obligatòriament ordenats de forma que 

+ comencem obligadament amb el nivell 1,
+ el nivell 2 sempre està precedit per un nivell 1,
+ el nivell 3 sempre està precedit per un nivell 2,
+ etc.

Mai _saltem_ de nivell, per exemple baixant directament del nivell 1 al nivell 3.
El que sí podem fer és pujar lliurement de nivell: per exemple, passant del
nivell 3 al nivell 1.

## Presentació

Els editors WYSIWYG permeten aplicar qualsevol canvi en la presentació de text,
i molts usuaris aprenen a seleccionar fragments de text per, a continuació,
aplicar directament presentació (negreta, cursiva, mides de les lletres, etc.).

Nosaltres evitarem al màxim aquesta forma de treballar, i en lloc d’aplicar
directament format al text seleccionat, el que farem és 
associar les pàgines o paràgrafs a estils. D’aquesta forma diferents
pàgines i diferents paràgrafs, fins i tot situats en diferents documents,
poden compartir estil, permetent així fer documents més homogenis, coherents
i amb la mateixa presentació.

Per definir la presentació del document el grafista disposa dels diferents
tipus de fulls d’estil (nosaltres ens concentrarem en els tres primers tipus):

+ **Estils de pàgina**: Utilitzeu els estils de pàgina per organitzar l’estructura
  del document i per afegir els números de pàgina. També podeu especificar
  l’estil de pàgina que s’aplicarà a la primera pàgina posterior a un salt de
  pàgina.
+ **Estils de paràgraf**: Utilitzeu els estils de paràgraf per formatar els
  paràgrafs, inclòs el tipus de lletra i la mida. També podeu seleccionar
  l’estil de paràgraf que aplicareu al paràgraf següent.
+ **Estils de caràcter**: Utilitzeu els estils de llista per formatar caràcters
  únics o paraules i frases senceres. Si ho voleu, podeu incrustar els estils
  de caràcter.
+ **Estils de marc**: Utilitzeu els estils de marc per formatar els marcs de text i
  de gràfic.
+ **Estils de llista**: Utilitzeu els estils de numeració per formatar les llistes
  numerades o amb pics.

## Pràctiques

Juntament a l’estudi de les explicacions anteriors, i que et caldrà llegir
reiteradament de forma alternada a la realització de les pràctiques següents, no
deixis de prémer la tecla `F1` per consultar l’ajuda interactiva del
_LibreOffice Writer_.

### Configuració del _LibreOffice Writer_

Per fer aquestes pràctiques cal que configuris el _LibreOffice Writer_,
utilitzant les opcions del menú _Insereix_ que siguin necessàries, de forma
que:

+ Ha de quedar visible una sola barra d’eines: la _Estàndard_.
+ No s’han de veure les regles, ni l’horitzontal ni la vertical.
+ Cal visualitzar _sempre_ el _Navegador_ (tecla `F5`). Fixa aquesta barra lateral en
  l’esquerra de la finestra per tenir-la sempre a la vista.
+ Cal visualitzar sempre el gestor d’estils (tecla `F11`).  Fixa aquesta barra lateral
  en la dreta de la finestra per tenir-la sempre a la vista.

La finestra del _LibreOffice Writer_ ha de quedar per tant dividida,
permanentment, en tres àrees:

1. Navegador
2. Document a editar
3. Gestor d’Estils

**Important**: dedica temps a explorar aquesta configuració de la finestra,
les opcions disponibles en el Navegador i el gestor d’Estils, les explicacions
de l’ajuda interactiva del _LibreOffice Writer_, etc. Cal tenir familiaritat
amb l’entorn de treball per ser productiu!

**Important**: ignorarem el menú _Format_ i la barra d’eines equivalent! Sempre
treballarem d’una mateixa i única forma: configurant els estils en el gestor d’Estils i
associant aquest estils amb les pàgines, paràgrafs o cadenes de caràcters que
vulguem formatar!

### Pràctiques amb estils de pàgina

Els estils de pàgina permeten definir l’aspecte general de les pàgines. Un sol
document pot consistir en seccions amb diferents tipus de pàgina, cada una
d’elles amb les seves propietats. Això és fonamental per poder presentar de
forma diferenciada la portada, la taula de continguts, el text, etc.

Aquesta pràctica consisteix en crear un sol document en el que usarem diferents
estils de pàgina. Primer de tot ens cal saber com inserir una nova pàgina decidint
quin serà el seu estil:

1. Cal obrir el quadre de diàleg que tenim en el menú _Insereix_, _Més salts_,
   _Salt manual…_,
2. on triarem com a tipus de salt el **salt de pàgina**,
3. i triarem també un **estil de pàgina** predefinit en la llista desplegable,
4. i on també podem decidir si la numeració de pàgines s’ha de reiniciar.

El nostre objectiu és usar en un nou document, **i en aquest ordre**, 
aquests quatre estils de pàgina predefinits:

1. **Primera pàgina**: sense numerar, i utilitzant tan sols els estils de
   paràgraf _Títol_ i _Subtítol_.
2. **Índex**: on situar una taula de continguts generada automàticament (menú
   _Insereix_, _Índex i taules_, _Taules de continguts…_). Podem numerar
   aquesta secció a partir de l’1 (insereix aquesta numeració en el peu de
   les pàgines per visualitzar-la). **Important**: fins que no usem en les
   pàgines següents del document estils de paràgraf amb un nivell assignat
   aquesta taula de continguts no tindrà contingut.
3. **Estil per defecte**: pàgina normal. Podem numerar aquesta secció a partir
   de l’1.
4. **Horitzontal**: format per inserir, per exemple, imatges apaïsades
   (fes-ho!). La numeració de les pàgines ha de continuar la se la secció
   anterior.
5. **Estil per defecte**: nova pàgina normal.

Afegeix en el document el text que consideris necessari per ajudar a la seva
comprensió, però usa [llatí][IPSUM] on el text
no sigui significatiu.

**Important**: una vegada practicats a continuació els estils de paràgraf
estructurals, torna a aquesta pràctica per, una vegada afegits aquests
paràgrafs, recalcular la taula de continguts.

### Pràctiques amb estils de paràgraf

Els estils de paràgraf més importants, i amb els que estructurarem tot el
document, son els estils predefinits _Encapçalament 1_, _Encapçalament 2_, etc.
A continuació es presenten una sèrie de llistes niuades que volen representar
conceptualment un document amb paràgrafs formatats amb aquestes estils:

* Secció A (Encapçalament 1)
    + Secció A.1 (Encapçalament 2)
    + Secció A.2 (Encapçalament 2)
* Secció B (Encapçalament 1)
    + Secció B.1 (Encapçalament 2)
        * Secció B.1.1 (Encapçalament 3)
        * Secció B.1.2 (Encapçalament 3)
    + Secció B.2 (Encapçalament 2)
* Secció C (Encapçalament 1)
* …

Prenent com a model l’estructura representada en aquesta llista, crea un nou
document i gestiona la seva estructura usant els estils predefinits
_Encapçalament 1_, _Encapçalament 2_, etc. Entre aquests paràgrafs intercala
abundant text en [llatí][IPSUM]. **Important**: no et confonguis pensant que has
d’imitar l’aspecte de les llistes anteriors!

Aspectes a tenir en compte:

* El _Navegador_ del  _LibreOffice Writer_ és ara una eina fantàstica per
  reorganitzar aquestes seccions del document: observa com representa els nivells
  dels encapçalaments.
* Verifica que en el Navegador saps augmentar i disminuir el nivell
  dels paràgrafs.
* Verifica que en el Navegador saps moure, amunt o avall, seccions senceres amb
  totes les seves subseccions incloses.

### Pràctiques amb estils de caràcter

Els estils de caràcters s’apliquen a fragments de text breus, que formen part
de paràgrafs més grans, i que en tot el document volem presentar d’igual forma. Analitza
les propietats principals d’aquests estils de caràcter predefinits (per exemple
les mides, tipus de lletra, etc.):
 
* _Èmfasi_
* _Èmfasi fort_
* _Exemple_
* _Teletip_
* _Variable_
* _Entrada d’usuari_

### Pràctiques avançades

1. Crea un nou estil de pàgina, de nom `DUO`, consistent en dues columnes
   periodístiques. En aquests tipus de columnes el text flueix automàticament
   des del final d’una columna al’inici de l’altre.
2. Verifica que alguns estils estan lligats per una **relació d’herència**:
   modifica algun aspecte del tipus de lletra de l’estil _Encapçalament_ per
   verificar que la resta d’estils _Encapçalament 1_, _Encapçalament 2_, etc.,
   també manifesten aquests canvis.
3. Edita l’aspecte de l’estil de paràgraf _Estil per defecte_, del que hereten
   tots els altres estils de paràgraf, i verifica que els canvis es propaguen
   als altres estils.
4. Crea diferents tipus de llistes utilitzant el _estils de llista_ apropiats.
5. Crea un taula de dues columnes per presentar text paral·lel (això és diferent
   de les columnes periodístiques):
    + Agafa el text d’aquest exemple:
      <http://www.indiana.edu/~audioweb/T206/two-column.html>).
    + Formata el text utilitzant únicament estils (de paràgraf, de caràcter, de
      llista, de taula), mai format directe!
    + Atenció: en els paràgrafs on tot el text és en majúscules cal fer que
      aquesta característica depengui de l’estil (cal escriure normal, no tot en
      majúscules).


[IPSUM]: http://es.lorem-ipsum.info/
