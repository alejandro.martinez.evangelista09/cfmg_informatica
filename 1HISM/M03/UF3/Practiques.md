## Pràctiques

En alguns exercicis es demana implementar versions simplificades d’algunes
ordres populars del _shell_. Estudia primer cada ordre (prova-la en un
terminal, llegeix la pàgina de manual), i a continuació implementa-la amb _Python_.

Tots els _scripts_ demanats ha de mostrar el seu resultat en la sortida
estàndard.

### Pràctiques sobre els arguments en la línia d’ordres

Practica algunes tècniques per gestionar la llista `sys.argv`:

1. Fes un _script_ que concateni tots els seus arguments en una sola cadena, a
   mostrar com a sortida del programa en una sola línia.
2. Fes un _script_ que multipliqui els seus tres primers arguments.
3. Fes un _script_ que sumi tots els arguments rebuts.
4. Implementa una variant simplificada de l’ordre del _shell_ `echo(1)`.
4. Implementa una variant simplificada de l’ordre del _shell_ `yes(1)`.
5. Fes un _script_ que mostri els seus arguments (un per línia), en ordre invers
   (consulta la documentació de `range`).

### Pràctiques sobre les variables d’entorn

Practica algunes tècniques per gestionar el diccionari `os.environ`:

1. Fes un _script_ que finalitzi la seva execució si l’usuari actual no és `root`.
2. Fes un _script_ que finalitzi la seva execució si la variable d’entorn `PPID`
   (_parent PID_) no està definida.
3. Fes un _script_ que finalitzi la seva execució si la variable d’entorn `UID` 
   té un valor numèric inferior a `1000`.
4. Fes un _script_ que finalitzi la seva execució si la variable d’entorn `SHELL` no
   acaba amb la cadena `/bash`.
5. Implementa una variant simplificada de l’ordre del _shell_ `logname(1)`.

