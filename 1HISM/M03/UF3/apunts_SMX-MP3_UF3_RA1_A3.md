# Navegar dins dels documents de text, tecles CUA, cut&paste, search&replace, etc.

Els editors de text moderns han unificat en gran part el seu us del
teclat. Encara que podem trobar diferències, les tècniques presentades en
aquest document són aplicables a qualsevol editor actual.

## IBM Common User Access

Tots els editors moderns implementen les dreceres de teclat
[CUA](http://en.wikipedia.org/wiki/IBM_Common_User_Access) (IBM Common User
Access).

Per realitzar les pràctiques proposades en aquest document necessitaràs _text_.
Els professionals de la tipografia sovint es troben en la mateixa situació, i
fan servir text generat automàticament, per exemple en
[llatí][IPSUM]: fes tu el mateix.

### Navegació i selecció amb el teclat

Les tecles CUA més populars són les que permeten navegar per un document i
fer-hi seleccions mitjançant el teclat.

* Per moure el cursor, prem la tecla o la combinació de tecles adient de la
  taula següent.
* Per seleccionar els caràcters sota el cursor en moviment, manté premuda a
  més la tecla `Maj` mentre mous el cursor.

**Important**: practica totes aquestes tecles sense usar mai el ratolí, i
practica tant el desplaçament del cursor com la selecció de text.

```
+=======================+=======================+=======================+
| Tecla                 | Funció                | +tecla Ctrl           |
+=======================+=======================+=======================+
| Tecles de fletxa      | Mou el cursor un      | Mou el cursor una     |
| esquerra i dreta      | caràcter cap a        | paraula cap a         |
|                       | l’esquerra o cap a    | l’esquerra o cap a    |
|                       | la dreta.             | la dreta.             |
+-----------------------+-----------------------+-----------------------+
| Tecles de fletxa      | Mou el cursor una     |(Ctrl+Alt)             |
| amunt i avall         | línia cap amunt o cap | Mou el paràgraf actual|
|                       | avall.                | amunt o avall.        |
+-----------------------+-----------------------+-----------------------+
| Inici                 | Mou el cursor al      | Mou el cursor al      |
|                       | principi de la línia  | principi del          |
|                       | actual.               | document.             |
+-----------------------+-----------------------+-----------------------+
| Fi                    | Mou el cursor al      | Mou el cursor al      |
|                       | final de la línia     | final del document.   |
|                       | actual.               |                       |
+-----------------------+-----------------------+-----------------------+
| Re Pàg                | Desplaça el document  | Mou el cursor a la    |
|                       | una pàgina cap amunt. | capçalera.            |
+-----------------------+-----------------------+-----------------------+
| Av Pàg                | Desplaça el document  | Mou el cursor al peu  |
|                       | una pàgina cap avall. | de pàgina.            |
+-----------------------+-----------------------+-----------------------+
```

### Cut & Paste

Una vegada hem seleccionat text podem realitzar les següents operacions:

1. Eliminar el text seleccionat.
2. Copiar el text en el _clipboard_ (el text seleccionat és copiat en un espai
   de memòria especial).
3. Combinar les dues accions anteriors en una.

Una vegada hi ha text en el _clipboard_ el podem enganxar en dos situacions diferents:

1. Enganxar a sobre de text seleccionat, que serà substituït.
2. Inserir en un punt del text (sense cap selecció activa).

Explora totes aquestes operacions utilitzant tan sols tecles. Què fa cada una
d’aquestes tecles (amb i sense text seleccionat)?

+ `Supr`
+ `Maj+Supr`
+ `Maj+Insert`
+ `Control+Insert`
+ `Control+C`
+ `Control+V`
+ `Control+X`

## Cerca i substitució

Saber fer cerques i substitucions és una de les tècniques més productives quan
usem editors de text. En particular, cal fer l’esforç d’aprendre a usar les
_expressions regulars_, tant per cerques com substitucions. El _shell_ d’Unix
ha popularitzat les expressions regulars gracies a eines com `grep` o `sed`C, i
ara tots els editors de text moderns les incorporen.

Practica amb _LibreOffice Writer_ tots els exemples dels següents tutorials, en
un document de text qualsevol, per exemple un generat en
[llatí][IPSUM].

+ [Opciones para buscar y reemplazar texto](https://wiki.openoffice.org/wiki/ES/Manuales/GuiaAOO/BuscarReemplazar)
+ [Usando expresiones regulares en Buscar o en Buscar - Reemplazar en Writer](https://blog.open-office.es/writer/usando-expresiones-regulares-en-buscar-o-en-buscar-reemplazar-en-writer)
+ [Expresiones regulares](https://wiki.openoffice.org/wiki/ES/Manuales/GuiaAOO/TemasAvanzados/RegExp)
+ [Documentation/How Tos/Regular Expressions in Writer](https://wiki.openoffice.org/wiki/Documentation/How_Tos/Regular_Expressions_in_Writer)
+ [Example regular expressions for Writer](https://www.oooninja.com/2007/12/example-regular-expressions-for-writer.html)
+ [Backreferences in replacements](https://www.oooninja.com/2007/12/backreferences-in-replacements-new.html)
+ [Powerful text matching with regular expressions](https://www.oooninja.com/2007/12/powerful-text-matching-with-regular.html)

[IPSUM]: http://es.lorem-ipsum.info/

