# coding: utf8
# Nom:      input_lista.py
# Autor:    Martinez Evangelista, Alejandro

# Vamos hacer una lista de nuestras asignaturas como ejemplo.

#asignatura = ["M01_Hardware", "M02_Sistemas_operativos", "M03_Ofimatica", "M05_Redes_locales", "M10_Empresa_e_inovacio_Emprendedora" , "M09_Formacion_y_orientación_laboral","Tutoria"]
#print("Yo estudio:")
#for x in range(len(asignatura)):
#   print("\t",asignatura[x])

#Ofimatica= "Ofimatica"
#Redes = "Redes"
#Hardware = "hardware"
#sistemas ="sistemas"
#asignatura = [ Ofimatica, Redes, Hardware, sistemas ]
#for x in range(len(asignatura)):
#    print(asignatura [x])

# Vamos con el .append para añadir un otra asignatura
#asignatura.append("Tutoria")

# Y con .pop podemos borra una asignatura
# Hay 3 maneras de utilizar el pop

    # Borrar un elemento de la lista por indice 
#asignatura.pop(1)

    # Borrar un elemento de la lista por argument
#asignatura.pop("Mates")
    # 

# Agrupar una Lista con otra lista.
#asprimer=["M01_Hardware", "M02_Sistemas_operativos", "M03_Ofimatica", "M05_Redes_locales", "M10_Empresa_e_inovacio_Emprendedora" , "M09_Formacion_y_orientación_laboral","Tutoria"]
#assegon=["Seguretat", "Ingles", "Programació"]
#cicle= asprimer.extend(assegon)
#print(cicle)

# Endreçar llistes
# Vamos hacer un sorteo de 6 numeros
#  
NumeroTotal = []
for x in range(6):
    NumeroTotal.append(int(input("introduzca 6 numeros: "))) 
NumeroTotal.sort()
print("Los numero son" + str(NumeroTotal))

if NumeroTotal == 912345:
    print("eres el ganador")
else:
    print("No eres el ganador")