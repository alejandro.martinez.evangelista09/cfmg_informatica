# NAME:         Calculadora.py
# Author:       Martinez evangelista, Alejandro
# Descripcion:  Fer un a calculadora que calcules les formules. 
import sys


num1 = float(sys.argv[1])
signo = sys.argv[2]
num2 = float(sys.argv[3])

#assert type(float(sys.argv[1])) is float, "ERROR: La 1r variable esta mal"

assert num2 > 0 , "No puede calcular numeros"
# Una assert es un comprobador de algun tipus. 
#assert type(num2) is float, "ERROR: La 2n variable esta mal"
# Aún da problemas al ejecutarlo en la terminal.s

if signo == '+':
    print(num1,"+",num2)
    print(num1 + num2)
    print("Resultado")
elif signo == '-':
    print(num1,"-",num2)
    print(num1 - num2)
    print("Resulatado")
elif signo == '*':
    print(num1,"*",num2)
    # Cuidado con el * porque es un parametro reservado. Entonces seria "python3 Calculadora 1 "*" 2 o python3 Calculadora.py 2 \* 1."
    print(num1 * num2)
    print("Resultado")
elif signo == '/':
    print(num1,"/",num2)
    print(num1 / num2 )
    print("Resultado")
   