# Autor:      Alejandro Martinez Evangelista 
# Nombre:        input_int.py
# Descripció: vamos a practicar con el input.

#Con esta linia vamos a pedir al que lo ejecute que introduzca su edad con el "int(input())"
edad = int(input("Introduce tu edad: "))# Con int(input()) Podemos poner numeros enteros

# Vamos hacer 3 variables en string 20 letras aleatorias.
letra1= input("pon 20 letras aleatorias: ")
letra2= input("pon 20 letras más: ")
letra3= input("pon 20 letras más: ")
print(letra1,letra2,letra3)
#Cadena= "Avui es divendres" # si podemos en el dentro de un print (Cadena [0:1]) podemos poner solo los caracteres dentro de un argumento.