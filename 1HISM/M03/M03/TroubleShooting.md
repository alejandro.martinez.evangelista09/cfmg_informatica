## Tot ok?

Recorda que per saber si la xarxa funciona el més simple és fer **ping** a
alguna màquina:

	$ ping gandhi	# verifica xarxa local
	$ ping seat.es	# verifica sortida a Internet

Per estar segur de tenir muntats els directoris del servidor pots executar en
un terminal aquestes ordres (si no hi ha sortida tenim problemes...):

	$ mount | grep $LOGNAME		# el teu directori
	$ mount | grep gandhi		# dos directoris: el teu i /home/groups

Si no s'ha muntat /home/groups pots provar en un terminal, com a _root_, a
executar:

	# mount -a

Si el que no s'ha muntat és el teu directori en el servidor prova aquesta ordre
com a _root_ (amb les paraules en majúscula substituides pel teu grup i usuari):

	# mount gandhi:/home/users/inf/GRUP/USER /home/users/inf/GRUP/USER/USER



















































# Exercici 1

Aquest exercici posa en pràctica el sistema de lliurament  recollida
d'exercicis.

## Exercici

Còpia aquest fitxer en un directori de treball, i escriu a continuació el teu
nom i la data actual en format ISO.

*Nom*:

*Data*:

Per acabar has de copiar aquest fitxer en un directori de nom MP3UF4 situat en
el teu directori del servidor.  Per crear el directori executa en un terminal:

	$ cd
	$ cd $LOGNAME
	$ mkdir MP3UF4
	$ cd MP3UF4
	$ pwd	# ha de mostrar una ruta similar a /home/users/inf/GRUP/USER/USER

Ara còpia en el directori $HOME/$LOGNAME/MP3UF4, sense canviar el seu nom,
aquest mateix fitxer.
