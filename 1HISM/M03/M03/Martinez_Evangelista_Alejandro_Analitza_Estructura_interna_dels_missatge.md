# Analitzar l’estructura interna dels missatges

Breu introducció al protocol SMTP, i en particular al format dels missatges de
correu.

## Descripció

El correu electrònic és un servei que ofereix Internet i que permet la comunicació
entre diversos usuaris d’una manera asíncrona (no cal que el que tramet i el que rep
estiguin connectats alhora). Aquests sistemes asíncrons es consideren en ocasions com
els [més productius](https://doist.com/blog/asynchronous-communication/).

## Documentació

El protocol SMTP va ser el primer gran protocol de comunicació personal, i va
establir les bases d’altres protocols que han definit l’Internet actual.

- <https://ca.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol>
- <https://es.wikipedia.org/wiki/Protocolo_para_transferencia_simple_de_correo>
- <https://www.ietf.org/rfc/rfc822.txt>
- <https://www.rfc-editor.org/rfc/rfc822.html>

Els missatges de correu consisteixen en simple text, organitzat de la següent
forma:

- El text es divideix en aquestes parts:
	1. Primer apareixen diverses _capçaleres_ (en anglès _headers_).
	2. A continuació una línia en blanc.
	3. I la resta del text constitueix el _cos_ (en anglès _body_).
- Les capçaleres més importants, i que has de recordar i entendre perfectament,
  són aquestes:
	- `Subject:` (el tema del missatge)
	- `From:` (el remitent del missatge)
	- `To:` (el destinatari del missatge)
	- `Date:` (la data de creació del missatge)
	- `Cc:` (_carbon copy_: <https://ca.wikipedia.org/wiki/Còpia_en_carbó>)
	- `Bc:` (_Blind carbon copy_)

## Pràctiques

1. Analitza aquest missatge. Detectes res d’estrany?

```
Delivered-To: jord...@gmail.com
Received: by 2002:a67:cc02:0:0:0:0:0 with SMTP id q2csp2511800vsl;
        Sat, 31 Aug 2019 14:44:26 -0700 (PDT)
X-Google-Smtp-Source: APXvYqwx4pXTQHkaj/3is8q+y1xOFvPkzLN4ioplOAWXdVxPXljJN/EVe2DZXL7KAucPaUyZwpQq
X-Received: by 2002:a62:d143:: with SMTP id t3mr22148787pfl.213.1567287865959;
        Sat, 31 Aug 2019 14:44:25 -0700 (PDT)
ARC-Seal: i=1; a=rsa-sha256; t=1567287865; cv=none;
        d=google.com; s=arc-20160816;
        b=pGSPa5a7AlHdPWTRtfDwaba6yvZJIrpCtqZxqfRPx8DmjDZHyZfCdH2zEtFIPwGps/
         OG83ajcOGUSeo7TwOi5iQKyIfkpv7hib2KhkjeniHC3WOJCIAFTHJTdOatuF+JFhRTXp
         LfsqM1LjlvYy3wuLxEb5+5hCf+GPwNkwFTxh+170B4iKjlugaOOlVdaOOVLwXufW2VCJ
         JxWLzBoDkSKc+uAqsTbaR3iWBa/dF+wzIrS/b18Ef20IOd0bmQRJ8WoTxPqXenKrqyd9
         8IuDtavti3BMSB26uPOh5/cciGflMwqZ/lO99VH9eqmGXWUJygzoJOw3ZqNC6m/ds+N7
         wECA==
ARC-Message-Signature: i=1; a=rsa-sha256; c=relaxed/relaxed; d=google.com; s=arc-20160816;
        h=content-transfer-encoding:mime-version:subject:message-id:to:from
         :date;
        bh=WOgMcffI+imxTE8xqVIhjE90VOQpSM6qxPuYHmsYHYI=;
        b=l4jyM0F3MHLzWpBHUE+RwMw1S/xmWLVma9hrzK0ScoZENw6XzTxYdZsG9MV485pKwx
         l3DPuo2eWTdGVo+MdZzFU8nZ+1fHwxi1XL9hPeXEhMyqw2CzNHKpD8XL0JnhXuIjVT7x
         BsbiXJJMyjGNoz4DybZsp8PK47yXZC4YOvqFhyrEeHR9ULLhwhEDLIdblQtNnFLAjkau
         hxaOZ/vynV/LmDCrPtF6kEkZ61ZsA/RY11jdySb8GulGm/OXFeYZfAbALByHQE6mOgl+
         awcwJQvJl1yafZ/tQa5ppAPPVxOi/LEW2m7iBF0Bkk3panAwbTbSxkA9jzHkLTnXxI42
         k2Zw==
ARC-Authentication-Results: i=1; mx.google.com;
       spf=softfail (google.com: domain of transitioning desmo...@gmail.com does not designate 2001:2e8:703:0:300::1000 as permitted sender) smtp.mailfrom=desmo...@gmail.com;
       dmarc=fail (p=NONE sp=QUARANTINE dis=NONE) header.from=gmail.com
Return-Path: <desmo...@gmail.com>
Received: from smtp.MYNET.JP (testsmtp.FreeBit.NET. [2001:2e8:703:0:300::1000])
        by mx.google.com with ESMTPS id x26si10000246pfn.78.2019.08.31.14.43.58
        (version=TLS1_2 cipher=ECDHE-RSA-AES128-GCM-SHA256 bits=128/128);
        Sat, 31 Aug 2019 14:44:25 -0700 (PDT)
Received-SPF: softfail (google.com: domain of transitioning desmo...@gmail.com does not designate 2001:2e8:703:0:300::1000 as permitted sender) client-ip=2001:2e8:703:0:300::1000;
Authentication-Results: mx.google.com;
       spf=softfail (google.com: domain of transitioning desmo...@gmail.com does not designate 2001:2e8:703:0:300::1000 as permitted sender) smtp.mailfrom=desmo...@gmail.com;
       dmarc=fail (p=NONE sp=QUARANTINE dis=NONE) header.from=gmail.com
Received: from isp-um00v-prd-g.localdomain (openstack.FreeBit.NET [219.99.80.130]) by smtp.MYNET.JP (8.14.7/8.14.7) with ESMTP id x7VLhuIi022056; Sun, 1 Sep 2019 06:43:56 +0900
Date: Sun, 01 Sep 2019 06:43:56 +0900
From: Kofi Sarpong <desmo...@gmail.com>
To: in...@yahoo.com
Message-ID: <201909010643...@gmail.com>
Subject: BUSINESS PROPOSAL
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-2022-JP
Content-Transfer-Encoding: 7bit
X-Client-IP: 197.220.169.137
X-Originating-Email: m-h...@po1.kannet.ne.jp
X-Mailer: UbiqMail Ver 3.0.0



Good Day,

my name is Kofi Sarpong, I am an official of a bank in Ghana with a long service experience.

I solicit your cooperation on behalf of an investor, to receive a    deposit of $12.5 million United States dollars in our suspense account   for investment in your country, with a 60/40 sharing benefits on the    revenue. I would make available to you necessary document that would    facilitate your application as beneficiary of the funds. Funds would be   released to you once your details are verified by our compliance unit.

All arrangement will be done on your behalf to release the funds into  your account. Your information will be protected and handled privately  with no risk involved. Please get back to me for further clarifications  and directives on the process.Please contact me with this email address: kko...@aol.com

Your cooperation is highly appreciated

Regards,

Kofi Sarpong 
```
* El remitent es diferent.
* Missatge esta incriptat.
* Falta el cos de carbo.

2. Analitza ara missatges que hagis rebut tu, ja sigui personalment o en la llista del grup.
~~~
Fwd: [MP3UF0] Configuració entorn de treball
Recibidos
	x
Alejandro.M. Evangelista <alejandro.martinez.evangelista09@gmail.com>
	
lun., 16 sept. 12:57
	
para mí


El lunes, 16 de septiembre de 2019, 12:04:09 (UTC+2), Joan Josep Ordinas Rosa escribió:

    # Configuració de l’entorn de treball

    Abans de res cal conèixer i configurar l’entorn de treball, eines de
    comunicació, etc.

    ## Descripció

    Aquesta activitat explora tres entorns:

    * Gestor d’escriptori _GNome_.
    * Correu electrònic.
    * Full de càlcul de _GSheets_ per mantenir registre d’activitats.

    Seguint la màxima “eat your own dog food” usarem la llista de
    correu del grup per tot el que sigui necessari, des de el primer dia del curs:
    lliurar exercicis, documents, plantejar dubtes, etc. Podem prefixar sempre el
    tema del missatge amb l’identificador de l’activitat, com ara
    `[MP3UF1RA1A2]`, etiquetar apropiadament els missatges en la web del grup de
    correu, etc.

    Cal afegir que disposar de paper i llapis o similar s’ha de considerar un _entorn_
    de treball necessari i obligatori ;-)

    ## Documentació

    Per accedir a determinats serveis de _Google_ farà falta disposar d’un compte
    de correu de _GMail_.

    ## Pràctiques

    1. Explora el gestor d’escriptori _GNome_:
    	- Compte d’usuari. Inici de sessió.
    	- Gestió de finestres.
    	- Tecles per dimensionar finestres.
    	- Configuració de les dreceres de teclat de _GNome_.
    	- Execució d’aplicacions.
    	- Ajuda de _GNome_ amb _Yelp_.
    	- Us del navegador _Firefox_.

    2. Si cal crea per a tu un compte de correu, per exemple amb <http://gmail.com>.

    3. Si cal configura el teu compte de correu per el remitent sigui clar i
       correcte.

    4. Subscriu-te via Web al grup (llista de correu) que et correspongui:
    	- <https://tinyurl.com/1hism2019>
    	- <https://tinyurl.com/1jism2019>

    5. Escriu el teu missatge de presentació al teu grup, ara des del client de correu:
    	- hism1_2...@correu.escoladeltreball.org
    	- jism1_2...@correu.escoladeltreball.org
    	- Una vegada tothom estigui subscrit rebrà aquest mateix document
    	  en un nou missatge del professor amb el prefix `[MP3UF0]` en el tema.
    	  Verifica-ho.

    6. Explora la interfície Web del grup:
    	- <https://tinyurl.com/1hism2019>
    	- <https://tinyurl.com/1jism2019>

    7. Accedeix amb _Firefox_ a [GSheets](http://sheets.google.com/), utilitzant el
       teu compte de _Google_, per crear un registre personal activitats:
    	- Usarem un nou full de càlcul amb tan sols dues columnes.
    	- Selecciona la columna **A** i aplica un format de data.
    	- Escriu la data actual en la cel·la **A1** amb el format `YYYY-MM-DD`.
    	- En la cel·la **A2** escriu la formula `=A1+1`.
    	- Copia la cel·la **A2** en la cel·la **A3**.
    	- En la columna **B** escriu les teves anotacions. Pots començar amb la
    	  corresponent al dia d’avui.
    	- Opcional:
    		- Elimina les columnes de la **C** fins al final.
    		- Afegeix primera fila amb noms per les columnes: `DATA` i
    		  `COMENTARI`.

    8. Recorda que és obligatori en aquesta assignatura mantenir al dia aquest
       registre d’activitats, amb una fila per cada dia de classe.
~~~
* Te el nom del remitent
* La seva capsalera.
* te cos en format markdown.
* 
3. Quins protocols d’Internet asíncrons coneixes? Quins de síncrons?
