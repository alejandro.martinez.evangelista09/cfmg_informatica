# coding: utf8 
# Name:        pizza.py
# Autor:       Marintez Evangelista, Alejandro
# Date:        04/03/2020
# Descripció:  Farem un script utilitzam la mateix forma que la pizza.py pero fer-lo amb una llista

def main():
    # Lista de ingredientes
    ingredient_no_vegetariano=["Jamon","Pepperoni","Pollatre","Bacon", "Chorizo"]
    ingredient_vegetariana=["Pimiento", "Tofu", "Champinion", "piña", "Rucula"]

    # Elegir la pizza
    Tipo= input("\nQue tipo de pizza quiere vegetariana o no vegetariana: ")

    if (Tipo.lower() == "vegetariana"):
        print ("\nEstos son los ingredientes pizza:")
        for x in range(len(ingredient_vegetariana)):
            print (x,"\t" ,ingredient_vegetariana[x])
        print("Selecciona 3 ingredientes:")
        ordren = ["Mozzallera" , "Tomate"]
        for x in range(3):
            ordren.append(input())
        print("Esta es tu pizza")
        print("Pizza", Tipo)
        print("Lleva", str(ordren))
    else:
        print ("\nEstos son los ingredientes pizza:")

        for x in range(len(ingredient_no_vegetariano)):
            print (x,"\t",ingredient_no_vegetariano[x])
        print("Selecciona 3 ingredientes:")
        ordren = ["Mozzarella","Tomate"]
        for x in range(3):
            ordren.append(input())
        print("Esta es tu pizza")
        print("Pizza", Tipo)
        print("Lleva", str(ordren))

main()