# Consultar ajuda interactiva

Presentació dels diferents tipus d’ajuda interactiva disponibles.

## Descripció

Aquesta activitat s’ha de fer intercalada amb la primera, i si cal
s’ha de revisar regularment per insistir en la importància de saber
usar l’ajuda interactiva.

## Documentació

- <https://github.com/thonny/thonny/wiki>
- <https://docs.python.org/3/>
- <https://www.w3schools.com/python/>
- <https://ca.wikipedia.org/wiki/GitHub>

## Pràctiques

Aquest document ha de ser lliurat per correu als alumnes prèviament a l’inici
de la classe. El tema del missatge estarà prefixat amb la cadena `[MP3UF4RA1A2]`.

Recorda que és obligatori en aquesta assignatura mantenir al dia el registre
d’activitats, amb una fila en el full de càlcul per cada dia de classe.

1. Consulta ajuda interactiva de _Thony_.
	- Consulta la ajuda local de _Thonny_.
	- Consulta la ajuda de _Thonny_ en el seu Wiki a _GitHub_ (saps que és
	  _GitHub_?).

2. Consulta ajuda interactiva de _Python_.
	- Executa el **IDE** i en el seu “Shell” executa la funció `help()` 
	- Usa la funció `help()` amb arguments. Per exemple: `help(pow)`.
	- Compara `help(8.0)` i `help(8)` (usa les tecles `Pag ▲` i `Pag ▼` per
	  visualitzar tot el text generat).
	- Explora el sistema d’ajuda de _Python_ en la seva Web.

3. Us de _Yelp_ per consultar pàgines de manual i la documentació d’aplicacions:
	- Obre _Yelp_.
	- Prem les tecles `CTRL-L`.
	- Sobreescriu completament la línia de text amb la URL `man:thonny` o
	  `man:python`.
	- Obre una nova finestra de _Yelp_ amb les tecles `CTRL-N`.
	- Fes cerques en una secció concreta de les pàgines de manual amb URLS
	  com ara `passwd(5)` o `passwd(1)`.
	- Obre el navegador de fitxers _Nautilus_ i prem la tecla `F1`.
	- Obre el terminal de _GNome_  i prem la tecla `F1`.

4. Explora el tutorial de _Python_ de la web
   [W3Schools](https://www.w3schools.com/python/).

5. Instal·la el paquet de documentació de _Python_ per així disposar localment
   de la documentació sense necessitar accés a Internet:
	- Com administrador executa `dnf install python3-docs`.
	- Consulta amb _Firefox_ la documentació local: on està?
		- Ho pots investigar amb aquesta ordre en el terminal `rpm -ql python3-docs | less`
		- La pàgina d’entrada està a <file:///usr/share/doc/python3-docs/html/index.html>
		- Important: registra aquesta pàgina local en el _Bookmarks_ de _Firefox_.

6. Configura _Firefox_ per que transformi els fitxers _MarkDown_, com
   ara aquest mateix, a HTML.
	- Afegeix a _Firefox_ l’_add-on_ _Markdown Viewer Webext_.
	- En la documentació d’aquest afegit situada a _GitHub_ explica com
	  modificar la base de dades MIME de _Gnome_ per que _Firefox_
	  identifique aquests fitxers correctament.

7. Com alternativa a Firefox pots usar l’editor _Geany_ per visualtzar els
   fitxers _Markdown_ convertits a una presentació tipogràfica:
	- Instal·la l’editor com administrador: `dnf install geany geany-plugins-markdown`.
	- Ja pots usar-lo amb fitxers _Markdown_: `geany A2.md&`.
	- Si cal, activa el _plugin_ de _Markdown_ en el gestor de _plugins_ (menú **Tools**).
	- Configura l’editor per visualitzar la previsualització de _Markdown_
	  en la _sidebar_. Dimensiona apropiadament les finestres.
