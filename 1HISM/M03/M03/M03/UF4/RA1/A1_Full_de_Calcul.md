# Operar amb nombres: constants, variables, operadors, funcions

Introducció a les expressions bàsiques en els llenguatges de programació.

## Descripció

A fi de preparar els alumnes per que usin expressions aritmètiques en qualsevol
situació (fulls de càlcul, bases de dades, etc.) aquesta activitat usarà el
llenguatge de programació _Python_ per introduir aquests coneixements
transversals.

## Documentació

Popularitat de _Python_:

- <https://www.techrepublic.com/article/python-is-eating-the-world-how-one-developers-side-project-became-the-hottest-programming-language-on-the-planet/>

Entorn integrat de desenvolupament (**IDE**):

- <https://thonny.org/>
- <https://docs.python.org/3/library/idle.html>

Preferentment usarem _Thonny_ per ser una aplicació multiplataforma, però si es
presenten problemes amb _Thonny_ usarem _IDLE_ amb _Fedora_ ja que es tracta
d’una aplicació molt més madura i robusta.

Propietats aritmètiques:

- <https://ca.wikipedia.org/wiki/Propietat_associativa>
- <https://ca.wikipedia.org/wiki/Ordre_de_les_operacions>

Nombres, operadors i variables amb _Python_:

- <https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-complex>
- <https://en.wikipedia.org/wiki/Python_syntax_and_semantics#Operators>

## Pràctiques

Aquest document ha de ser lliurat per correu als alumnes prèviament a l’inici
de la classe. El tema del missatge estarà prefixat amb la cadena `[MP3UF4RA1A1]`.
Aquesta codificació es farà servir en molts dels missatges al llarg del curs.

Recorda que és obligatori en aquesta assignatura mantenir al dia el registre
d’activitats, amb una fila en el full de càlcul per cada dia de classe.

1. Instal·la el **IDE**:
	- Per disposar de _Thonny_ i _IDLE_ amb _Fedora_: `dnf install thonny python3-idle`.
	- Visita i explora <https://thonny.org/>.
	- Instal·la _Thonny_ a casa teva en el teu ordinador personal.

2. Executa _Thonny_:
	- Dimensiona la finestra a pantalla sencera.
	- Verifica que visualitzes quatre seccions separades en la finestra de l’aplicació.
	- Explora opcions del menú “View”.
	- Prepara _Thonny_ per que les seccions visibles siguin “Editor”,
	  “Shell”, “Notes” i “Assistant”.

3. Abans de continuar amb aquesta activitat treballa la [A2](A2.md) (sistemes
   d’ajuda i documentació).
 
4. Explora l’evaluació d’expressions en el _Shell_ de _Python_:
	- Tipus numèrics de _Python_ i operadors.
	- Ordre d’evaluació dels operadors.
	- Associativitat dels operadors.
	- Tipus de divisió (entera i real).
	- Conversió entre enters i reals.

5. Realitza els exercicis proposats pels professors:
	- Escriu expressions on l’us dels parèntesis modifiqui el resultat.
	- Practica el tutorial <https://www.w3schools.com/python/python_numbers.asp>.
	- Practica el tutorial <https://www.w3schools.com/python/python_variables.asp>.
	- ...

## Com desar aquests documents

Per desar, a fi de consultar-los en el futur, tots els documents com aquest que
al llarg dels curs s’et proporcionaran, et recomanem una estructura de
directoris com aquesta:

	SMX-MP3
	├── UF1
	│   ├── RA1
	│   └── RA2
	├── UF2
	│   └── RA1
	├── UF3
	│   └── RA1
	├── UF4
	│   └── RA1
	├── UF5
	│   └── RA1
	└── UF6
	    ├── RA1
	    ├── RA2
	    └── RA3

La pots crear fàcilment amb aquestes ordres a executar en un terminal (la
primera ordre usa variables predefinides, i et situa en la teva carpeta compartida
del servidor):

	cd $HOME/$LOGNAME
	mkdir -p SMX-MP3/UF{1,2,3,4,5,6}/RA1
	mkdir -p SMX-MP3/UF{1,6}/RA2
	mkdir -p SMX-MP3/UF6/RA3

Aquest document en concret el pots deixar amb el nom `A1.md` en el directori `UF4/RA1`.