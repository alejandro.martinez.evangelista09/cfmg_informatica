# Exercici 1

Escriu a continuació el teu nom i la data actual en format ISO.

*Nom*:Alejandro Martinez Evangelista

*Data*:2019-10-21
Per acabar has de copiar aquest fitxer en un directori de nom MP3UF4 situat en
el teu directori del servidor.

Per estar segur de tenir muntats els directoris del servidor:

	$ mount | grep $LOGNAME
	$ mount | grep gandhi

Per crear el directori:

	$ cd
	$ cd $LOGNAME
	$ mkdir MP3UF4
	$ cd MP3UF4

El resultat final consisteix en que en el directori $HOME/$LOGNAME/MP3UF4 hi
hagi una còpia d'aquest fitxer.

