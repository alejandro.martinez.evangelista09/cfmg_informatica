# coding: utf8 
# Name:        pizza.py
# Autor:       Marintez Evangelista, Alejandro
# Date:        04/03/2020
# Descripció:  Farem un script utilitzam la mateix forma que la pizza.py pero fer-lo amb una llista
def main():
    # Lista de ingredientes
    ingredient_no_vegetariano=["Jamon","Pepperoni","Pollatre","Bacon", "Chorizo"]
    ingredient_vegetariana=["Pimiento", "Tofu", "Champinion", "piña", "Rucula"]

    # Elegir la masa
    ingrediente_base = "tomate y mozzarella"

    # Elegir la pizza
    Tipo= input("\nQue tipo de pizza quiere vegetariana o no vegetariana: ")

    if (Tipo.lower == "vegetariana"):
        print ("\nEsta es tu pizza con:")
        for x in range(len(ingredient_vegetariana)):
            print ("\t",ingrediente_base ,ingredient_vegetariana[x])
    else:
        print ("\nEsta es tu pizza")
        for x in range(len(ingredient_no_vegetariano)):
            print ("\t",ingredient_no_vegetariano[x])
main()