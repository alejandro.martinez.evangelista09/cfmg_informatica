def doble(n):
    """Retorna el doble de n"""
    return n + n
assert doble(8) == 16
assert doble(0) == 0