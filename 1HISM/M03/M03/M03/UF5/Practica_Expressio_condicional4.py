#coding:utf8

"""Activitat_3 Practiques de python:"""

# Apliquem funció sign.

def sign(n):
    "Retorna 1 si n > 0, -1 si n < 0, i 0 si n == 0"
    positiu = n > 0
    negatiu = n < 0
    return 1 if positiu else (-n if n<0 else 0)

# Millorem la funció aplican assert.

assert sign(33) == 1
assert sign(-7) == -1
assert sign(7-7) == 0

# Apliquem funció _abs i que retorni -n if n<0 else n .

def _abs(n):
    "retorni -n if n<0 else n"
    return -n if n < 0 else n

# Apliquem funció logiques que indiqui si un nombre entre en senar, parell o zero.

def parell(n):
    if parell(n): 
        return n %2 == 0
        "Es un numero parell"
    if parell(n):
        return n %2 != 0
    if parell(n):
        return n == 0
        "Es un zero"
    

