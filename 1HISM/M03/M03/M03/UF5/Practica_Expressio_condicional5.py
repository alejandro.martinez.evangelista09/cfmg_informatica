#coding:utf8

"""Activitat Practiques de python:"""

# Constants global


# Aquesta funció no existeix amb _Python_, però **sí** en el full de càlcul def sign(n)

def sign(n):
    """Retorna 1 si n > 0, -1 si n < 0, i 0 si n == 0"""
    positiu = n > 0
    negatiu = n < 0
    return 1 if positiu else (-1 if negatiu else 0)
# Millorem la funció aplican assert.

assert sign(33) == 1
assert sign(-7) == -1
assert sign(7-7) == 0

# Retorni si -n n<0 else n .

def _abs(n):
    """Retorni -n if n<0 else n"""
    return -n if n < 0 else n


# Retorna si un nombre entre en senar, parell o zero.

def parell(n):
    """ Retorna True si es parell i False si no o és"""
    return n %2 == 0 

assert parell(2) 
assert not parell(3)

def not_parell(n):
    """Retorna True si es senar."""
    return n %2 != 0

assert not_parell(3)
assert not not_parell(2)

def zero(n):
    """Retorna True si es zero. """
    return n == 0

assert zero(0)
assert not zero(1)
assert not zero(-1)

# Apliquem la funció isfloat i que retorni si es `True` o `False` el tipus de valor.

def isfloat(n):
    """ Retorna True o False segons si n sigui nombre real."""
    return type(n) is float

assert isfloat(2.1)
assert not isfloat(2)

# Apliquem la funcions `_max(a,b)` i `_min(a,b)` i que retorni el valor indicat.

def _max(a,b):
    """Retorna el valor numeric mes gran entre el `a` i el `b`"""
    return a if a > b else b

assert _max(2,1) == 2


def _min(a,b):
    """Retorna el valor numeric mes petit entre el `a` i el `b`"""
    return a if a < b else a

assert _min(1,2) == 1

#Apliquem la funció `max3` per que retorni el maxim de 3 numeros rebuts com parametre.

def max3(a,b,c):
    """Retorna el valor maxim de entre tres valors numerics (a,b,c)"""
    return a if a > b else (b if b > c else c)

assert max3(7,2,1) == 7
assert max3(0,2,1) == 2
assert max3(1,3,10) == 10


#Apliquem dos funcions per convertir graus entres les escales Celsius i Fahrenheit.

def Cel_Fah(n):
    """Retorna els grau Celsius (n) convertit en graus Fahrenheit"""
    return (n *9/5) + 32

assert Cel_Fah(0) == 32

def Fah_Cel(n):
    """Retorna els graus Fahrenheit (n) convertit en Graus Celsius"""
    return (n - 32) * 5/9

assert Fah_Cel(32) == 0


# Completa, i corregeix si cal, aquesta funció. No oblidis de fer servir assercions per depurar-la.

def segons_a_hora(segons):
    """Converteix segons a hores, minuts i segons restants"""
    h = segons // 3600
    x = segons % 3600
    m = x // 60
    s = x % 60
    return (h, m, s)

assert segons_a_hora(360) == (0, 6, 0)
assert segons_a_hora(3600) == (1,0,0)
assert segons_a_hora(36000) == (10,0,0)
assert segons_a_hora(3660) == (1,1,0)
assert segons_a_hora(0) == (0,0,0)
