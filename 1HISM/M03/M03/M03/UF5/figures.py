#coding: utf8

#Calcular la area de un triangle.

def triangle(b,a):
    """Retorna el calcul del àrea de un triangle."""
    return b * a / 2

assert triangle(24,7) == 84,0
