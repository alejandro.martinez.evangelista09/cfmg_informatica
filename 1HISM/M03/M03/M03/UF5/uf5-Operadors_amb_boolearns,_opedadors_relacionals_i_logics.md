# Operar amb constants booleanes, operadors relacionals i lògics

Introducció a la representació dels valors lògics i les seves operacions.

## Descripció

Per començar a treballar amb expressions lògiques ho farem amb el llenguatge de
programació _Python_ utilitzant l’entorn integrat de desenvolupament (**IDE**).

## Documentació

Aquestes URLs són de pàgines locals, disponibles en el teu sistema si has
instal·lat el paquet `python3-docs`.

- Constants: <file:///usr/share/doc/python3-docs/html/library/constants.html>
- Operacions: <file:///usr/share/doc/python3-docs/html/library/stdtypes.html#boolean-operations-and-or-not>
- Comparacions: <file:///usr/share/doc/python3-docs/html/library/stdtypes.html#comparisons>
- Coerció: <file:///usr/share/doc/python3-docs/html/library/stdtypes.html#truth-value-testing>

## Pràctiques sobre operadors relacionals

Nombres i cadenes es poden _comparar_. Com? I que en resulta d’una comparació?

1. Verifica amb **nombres**, practicant en el “Shell” del **IDE**, els resultats
generats per aquests operadors:
	- `==`
	- `!=`
	- `>`
	- `<`
	- `>=`
	- `<=`

2. Ara verifica amb **cadenes** de text els mateixos operadors. Com va amb les
   lletres accentuades? Per exemple, quin resultat genera l’expressió `"e" < "à"`.
	- Consulta la documentació de les funcions `chr`i `ord` amb la funció
	  interactiva `help`.
	- Saps trobar aquestes mateixes descripcions en la documentació web de
	  _Python_?

## Pràctiques sobre constants i operadors lògics

1. Escriu aquest programa en l’editor del **IDE**, executa’l pas a pas, i desa
   la seva sortida en la finestra “Notes” (o en forma de comentari en l’editor)
   pel seu posterior estudi:

		a = True and True
		b = True and False
		c = False and True
		d = False and False
		print(a,b,c,d)

2. Fes una variació del programa ara amb l’operador `or` en lloc de l’operador
   `and`. Desa la seva sortida en la finestra “Notes” i compara els resultats
   amb els de l’operador `and`.  És important que recordis aquests resultats.

3. Fes una variació del programa ara amb l’operador `not`. Important: aquest
   operador és _unari_!

4. Per entendre com estan implementats els valors lògics amb _Python_ executa
   en el “Shell” del **IDE** aquestes expressions (cal **entendre** el resultat!):
	1. `True == False`
	1. `True != False`
	1. `False == 0`
	1. `True == 1`
	1. `True is 1`
	1. `True + 11`
	1. `True = 2`

5. Verifica aquestes expressions (cal **entendre** el resultat i saber-ho explicar!):
	1. `not ""`
	1. `not " "`
	1. `not "X"`
	1. `not 0`
	1. `not 0.0`
	1. `not 1`
	1. `not 33`
	1. `"" == False`
	1. `"X" == True`

6. En el “Shell” del **IDE** executa `help(bool)` (llegeix tan sols la primera
   explicació)  i contesta:
	- Que fa la funció `bool`?
	- Quants arguments espera?
	- Quins tipus d’arguments suporta?

7. Consulta la documentació de la funció `bool` en la pàgina que descriu les
   [funcions predefinides](file:///usr/share/doc/python3-docs/html/library/functions.html#bool)
   de _Python_.

8. Verifica aquestes expressions:
	1. `bool(True + True)`
	1. `bool(True + False)`
	1. `bool(True * True)`
	1. `bool(True * False)`

9. Reescriu les expressions anteriors fent servir operadors lògics en lloc de la
   suma i la multiplicació.

