#coding:utf8

"""Activitat_2 Practiques de python:"""

# Apliquem funció sign

def sign(n):
    "Retorna 1 si n > 0, -1 si n < 0, i 0 si n == 0"
    positiu = n > 0
    negatiu = n < 0
    return 1 if positiu else (-n if n<0 else 0)

# Millorem la funció aplican assert

assert sign(33) == 1
assert sign(-7) == -1
assert sign(7-7) == 0