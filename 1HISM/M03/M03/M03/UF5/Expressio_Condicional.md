# Usar `if` expressió, definir funcions simples

Introducció a l’expressió condicional i la definició de simples funcions.

## Descripció

Seguirem treballant les expressions lògiques amb el llenguatge de
programació _Python_, ara amb l’ajuda de la construcció condicional `if`.
Aquesta expressió, encara que amb una sintaxi lleugerament diferent, serà molt
important en treballar amb fulls de càlcul.

## Documentació

Algunes d’aquestes URLs són de pàgines locals, disponibles en el teu sistema si has
instal·lat el paquet `python3-docs`.

- <file:///usr/share/doc/python3-docs/html/reference/expressions.html#conditional-expressions>
- <https://www.python.org/dev/peps/pep-0308/>
- <https://www.w3schools.com/python/python_functions.asp>
- <https://www.w3schools.com/python/python_casting.asp>

Plantilla a imitar sempre en definir nous mòduls (fitxers amb funcions):

	"""Primera línia, amb descripció general del mòdul.
	   Podem usar un comentari de més d’una línia.
	"""

	# Constants globals
	IVA = 0.21

	# Funcions
	def F1(p, q):
	    "F1 ... p ... q ..."
	    ...
	    return ...

	def F2(p, q):
	    "F2 ... p ... q ..."
	    ...
	    return ...

Exemple a imitar sempre en definir noves funcions:

	# Tradicional funció `sign`
	# Aquesta funció no existeix amb _Python_, però **sí** en el full de càlcul
	def sign(n):
	    "Retorna 1 si n > 0, -1 si n < 0, i 0 si n == 0"
	    positiu = n > 0
	    negatiu = n < 0
	    return 1 if positiu else (-1 if negatiu else 0)

## Pràctiques

Com que algunes de les funcions a definir _ja existeixen_ usarem en alguns
casos noms diferents o prefixarem els noms amb un guió baix.

1. Prepara un nou mòdul amb la funció `sign`. Executa'l i crida a `sign` en el
   “Shell” del **IDE**.

2. Millora la definició de la funció `sign` afegint, a continuació seva en el
   fitxer, assercions com aquestes:

	    	assert sign(33) == 1
	    	assert sign(-7) == -1
	    	assert sign(7-7) == 0

	Oi que sembla que aquest codi no faci res? Modifica ara la funció per
	que estigui **mal** definida i executa de nou el fitxer: que passa?
	Pots fer servir aquesta tècnica, si necessites ajuda, en totes les
	funcions que defineixis.

3. Defineix una funció de nom `_abs` que retorni el resultat d’aquesta
   expressió: `-n if n<0 else n`.

4. Defineix les funcions lògiques `parell` (_dit del nombre enter que és
   múltiple de dos_), `senar` (_dit del nombre enter que **no** és múltiple de
   dos_) i `zero` que determinin si el seu paràmetre és parell, senar o zero
   respectivament.

5. Defineix una funció lògica de nom `isfloat` que retorni `True` o `False`
   segons el paràmetre rebut sigui un nombre real (coma flotant) o enter.

6. Defineix les funcions `_max(a,b)` i `_min(a,b)` que retornen el que el seu
   nom indica. Funcionen igual amb nombres i cadenes?

7. Defineix una funció de nom `max3` que retorni el màxim de 3 nombres rebuts
   com a paràmetre. Usa variables locals per simplificar l’expressió final.

8. En un nou mòdul de nom `figures.py` defineix una funció per calcular l’àrea
   d’un triangle rectangle. Si et cal busca la formula matemàtica a Internet.
   En el mateix mòdul fes funcions similars per altres tipus de rectangles.

9. Repassa els apartats introductoris del tutorial <https://www.w3schools.com/python>.

10. Defineix funcions per convertir graus entres les escales Celsius i Fahrenheit:
   <https://ca.wikipedia.org/wiki/Grau_Fahrenheit>.

11. Completa, i corregeix si cal, aquesta funció. No oblidis de fer servir
    assercions per depurar-la.

		def segons_a_hora(segons):
		    "Converteix segons a hores, minuts i segons restants"
		    h = segons // 3600
		    x = segons % 3600   # variable auxiliar
		    m = x ...
		    s = x ...
		    return (h, m, s)

	Les assercions que han de comparar el valor retornat per aquesta funció
	han de ser com aquesta (amb les paraules en majúscules evidentment
	substituïdes per constants literals):

		assert segons_a_hora(SEGONS_TOTALS) == (HORA, MINUTS, SEGONS)
