# Operar amb cadenes: constants, operadors, funcions, mètodes

Introducció a les expressions de cadenes en els llenguatges de programació.

## Descripció

Com a introducció de les expressions de cadenes, a ser usades en qualsevol
situació (fulls de càlcul, bases de dades, etc.), aquesta activitat usarà el
llenguatge de programació _Python_ per introduir aquests coneixements
transversals.

Assumirem la codificació de text Unicode i que sempre fem servir _Python 3_.

## Documentació

Cadenes, operadors i variables amb _Python_:

- <https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str>
- <https://ca.wikipedia.org/wiki/Unicode>

### Definició de variables

Per simplificar expressions complexes farem servir _variables_, el sistema
de posar **nom** a valors generats per expressions. Exemples:

	PI = 3.1415926535
	radi = 3
	area = (2*PI*radi) ** 2

## Pràctiques

Aquest document ha de ser lliurat per correu als alumnes prèviament a l’inici
de la classe. El tema del missatge estarà prefixat amb la cadena `[MP3UF4RA1A3]`.

Recorda que és obligatori en aquesta assignatura mantenir al dia el registre
d’activitats, amb una fila en el full de càlcul per cada dia de classe.

1. Practica el tutorial <https://www.w3schools.com/python/python_strings.asp>.

2. Fes servir l'editor del **IDE** per assignar a variables cadenes multi-línia.
	- Defineix la variable.
	- Desa el text de l'editor en un fitxer (pot ser temporal, com ara `/tmp/x.py').
	- Avalua directament el contingut de l'editor amb la tecla `F5`.
	- Fes que es visualitzi la finestra “variables”.
	- Avalua la variable definida en el “Shell” del **IDE**.

3. Verifica que entens aquests conceptes i els pots explicar i posar exemples:
	- Representació de cadenes literals amb _Python_.
	- Concatenació de cadenes.
	- Subcadenes amb _slicing_.
	- Consultar la longitud de les cadenes amb la funció `len()`.
	- Conversió d'altres tipus a cadenes amb la funció `str()`.
	- Execució de mètodes amb cadenes.
	- Alguns importants mètodes de cadena:
		- `count`
		- `find`
		- `isalnum`, `isalpha`, etc.
		- `lower` i `upper`
		- `strip`, `lstrip` i `strip`
		- `replace`

4. Explora l'aplicació _GNome Characters_ per accedir a caràcters no presents en el teclat.
	- Caràcters tipogràfics més habituals:
		- …
		- « »
		- ‘ ’
		- “ ”
		- —
		- –