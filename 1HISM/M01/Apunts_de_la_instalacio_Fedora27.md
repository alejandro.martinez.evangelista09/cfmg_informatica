# Apunts de la instalació de fedora
## Resum
El divendres día 13 de Septembre, estaven aquell día per instal un fedora 27 al ordinador per medi de la xarxa (PXE) en el seu server propi que es diu Gandhi.


## Hardware

El hardware es la part fisica del ordinador. 

En la clase habia ordinadors (amb torre,monitor, teclat amb ratolí) tots en bon estat i revisats per els professors del cicle per poder instalar el fedora 27.

## Sotfware

En la primera hora ens van explicar sobre el metodes de instalació de un sistema operatiu(son amb CD/DVD, pendrive i xarxa(PXE)),que significava ".iso" (es instaldor de sistema operatiu).

Quant s'encen el ordenador pulsem el F12 abans que es poses el GRUB per entra en el boot menu (o Menu d'arrancada) i aixì arrencar per PXE i descarga el fedora 27 que estava en el Gandhi. Una vegada s'instala es mostra instrucions per elegir el idioma tant del teclat com de llengua, el nombre de root y usuari amb contraseña (diferent que la del root) i anar a la linia d'odres (amb Ctrl+alt+f2 fins al 6) per cambiar el idioma del teclat amb la comanda:

```
loadkeys es
```
## Sistema operatius

El Software és la part del programari del ordinador.

Els professors en havien explicat que hi ha molts O.S (Sistema operatiu) en el món de la informatica com el windows, el Linux , Mac, Fedora i Debian. Instalarem el fedora 27 amb GNOME com escritori en tots el ordinador.

## Particions del disc 
Els professors ens expliquen les taules de particions. Que són parts de disc que se utilitza per emagatzemar dades i posar O.S.

En la taula de particions sols es poden posar 4 particions primaries , 1 partición extesa i dins de la extesa les logiques, que espoden posar particions fins que el espai de la partició no tingui res de espai.

En la practica tenien que crear particions a la linia d'odres:
* 1 p.extesa
* 3 particiones dentro de la extesa(+100G+100G+5G)

Per entra al sda (o disc)
* 1r Es posar
```
fdisk /dev/sda
```

* 2n Crea una extended
```
 n
 e
 enter
 enter
 +205G
 ```
 * 3r Crea logiques (dos particions una pels alumnes que estan el mati i la altra pels de la tarde)
 ```
 n
 l
 enter
 enter
 +100G
 ```
 * 4r Crea el swap
 ```
 n
 l
 enter
 enter
 enter
 t
82
```
## Instalador 
El principi eleguiem:
```
 El idioma
 ```
 ```
 La contrasenya del root
 ```
 ```
 El idioma del teclat (amb la linia de comandes o no)
 ```
 ```
 El nombre d'usuari i la contrasenya 
 ```
 ```
 La taula de particions
 ```
 i després instalar-lo per comple en el ordinador i ja estaria.