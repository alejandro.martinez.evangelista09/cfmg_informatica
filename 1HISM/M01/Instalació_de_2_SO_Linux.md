# MBR: 2 Sistemes operatiu

* Per mirar la memoria es amb la comanda:
~~~
$ df -h
S.ficheros                          Tamaño Usados  Disp Uso% Montado en
devtmpfs                              3,9G      0  3,9G   0% /dev
tmpfs                                 3,9G    48M  3,8G   2% /dev/shm
tmpfs                                 3,9G   1,6M  3,9G   1% /run
tmpfs                                 3,9G      0  3,9G   0% /sys/fs/cgroup
/dev/sda5                              98G    54G   40G  58% /
tmpfs                                 3,9G   1,7M  3,9G   1% /tmp
tmpfs                                 787M    16K  787M   1% /run/user/42
gandhi:/groups                        931G   236G  696G  26% /home/groups
tmpfs                                 787M    48K  787M   1% /run/user/101644
gandhi:/users/inf/hism1/ism47916106   931G   236G  696G  26% /home/users/inf/hism1/ism47916106/ism47916106
~~~
**On posa */dev/sda* és on diu el tamany de la nostra memoria, el espai que em utilitzat i el que queda**

* Instalarem un fedora 27 de 10Gb de disc dur i un debian 9 2GB de disc, 3Gb de RAM y 2GB Gb de swap.

* Obrir el instalador del fedora 27 i crear les partició (10GB + 2GB +3GB = 15GB) 
**Hi ha dos formes de fer les particions per fdisk (sense entorn grafic) o amb gpated/en el mateix instalador**

* Després tenim que canviar el titol del menu del grub de *fedora (numero de versio) 27 (on apunta)* per *fedora27*.