# BIOS

## Descripció

**El bios es un sistema basic de entrada i sortida integrad en el chip ,que esta integrad en la placa,que comproba el dispositius conectats a la placa i després s'encarrega de arrancar el S.O.**

* Quant s'enguega el primer programa que es ejecuta es el de arrancada després van els altres.
* Les sigles de la BIOS volen dir Basic input/output System.
* Quant es parla de "la bios" no es una paraula femenina sino masculina per aixó es diu el BIOS.

## El ROM i el EPROM

* ROM es la memoria no volàtil i sols en lectura.

* EPROM es com el ROM de lectura, pero en canvi es pot borrar i programar desde zero.

* La memoria flash és similar l'EPROM pero es pot escriure sense borrar, es a dir, es d'escritura i lectura.

**Per actualitzar la bios a de tenir molt de conte perqué es un proces molt delicat i per el minim error es fa malbe.**

## Emprese de chip BIOS

Hi ha tres empreses que fabriquent BIOS.
* AMI
* Award
* Phonix
**La empresa Phonix va compra la empresa Award dons en la actualitat hi ha només dos empreses.**

## CMOS
* Es una memoria volàtil, que guarda la configuració de la bios.

## POST (power-on self-test)

* En el PC hi ha dos tipus de arrancada la freda que es fa quan pulses el botó i començant a encendre el procesos i serveix, i la calenta es que es torna arrancar pero desde punt que ya acabat de engegar el procesos.

**Es una sequencia tipica de missatge.**
* Mensagea amb els components 
* El nombre del fabricant
* El tipus de CPU i velocitat
* El test de la RAM
* Un missatge per accedir a la BIOS

Hay dos tipos de systemas de arranque.
* MBR-DOS (Legacy): Sols llegueix una partició del disc en un sector en especific.
* UEFI: Es més complex, busca un fitxer per arrancar dins de una partició que es diu EFI, pero té que estar la particionat en GPT i la partició EFI te que estar formatejada en FAT32/16.

overclocking: Es un programa en la BIOS per donar més velocitat i donar més rendiment, pero gasta més volts.

