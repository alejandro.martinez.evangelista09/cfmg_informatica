# Fonts d'alimentació
**Les fonts d'alimentació es un components que dona energia tot el HW, transforma la energia alterna en continua i manté al tensió en un nivell estable**
## Imatge

![Fonts de alimentació](https://images-na.ssl-images-amazon.com/images/I/51KMMloK5gL._SX466_.jpg)

## Connectors i tensió del fils (colors)
* Conectors de 24 pins ATX hi ha també de 20+4.
    * image

    ![cables](https://images-na.ssl-images-amazon.com/images/I/51176pYNazL._SX425_.jpg)
* EPS és un connector de 4 o 8 pins que va connectat el procesador porta corrent 12 V. Se combina amb el overcloking.
    * imatge

    ![EPS](https://i.ebayimg.com/images/g/90QAAOSwnHZYVncF/s-l300.jpg)
* En els conectors PCI-E, on es tranmeteix el dades amb més velocitat, te dos tipus de conectors de 6 pins i de 6+2 pins
    * imatge
    ![PCI-E](https://http2.mlstatic.com/cable-pci-e-6-pines-cable-molex-adaptador-tarjeta-grafica-D_NQ_NP_818659-MPE25609213803_052017-F.jpg)

## Tipus de colors en els cables

| Colors | Funció |
|--------|--------|
Negro| Ground. Tierra.|
Amarillo| +12v
Azul| -12v
Rojo| +5v
Blanco| -5v
Naranja| +3.3v
Verde| Power ON. Señal para encendido.
Gris| Power Good. indicador de funcionamiento de la fuente.
Morado| +5v en estado Stand By. 5v permanentes.

**Imatge**

![ATX](https://s2.studylib.es/store/data/006076165_1-7d29b4e688d7d5ba0d4de70f7bcda55f.png)

## Components elèctrics principals
* open power supply
![open power supply](https://images-na.ssl-images-amazon.com/images/I/710tOEzZJ6L._SL1500_.jpg)
    ### Components electrics
    * Transformadors: es un element electric que ajuda a pujar o baixar la tensió.

    ![Transformadors](https://cdn1.todoelectronica.com/181966-large_default/transformador-chasis-abierto-12va-2-x-12v-500ma.jpg)

    * Bobina: Són per fer camp magnetics per transforma la corrent alterna en continua.

    ![Bobina](https://i0.wp.com/www.ingmecafenix.com/wp-content/uploads/2017/04/bobina.jpg?ssl=1)

    * Condensadors: Serveixen per estabilitza la corrent.
    ![condensadors](https://i0.wp.com/www.teslaelectronic.com.pe/wp-content/uploads/2018/12/Cap.1000uf.35v.jpg?fit=676%2C500&ssl=1)
    
## Tester (polímetre)
* És un aparell se s'utilitza per medi la eletricitat i si el material que toca són conductor.

![tester](https://http2.mlstatic.com/tester-multimetro-digital-profesional-proskit-mt-1210-luz-D_NQ_NP_600300-MLA31007897486_062019-Q.jpg)

### Components 

* Cables (vermell per els voltaje i un negre per terre(COM))

* Pila

* Roda(per indicar que medir o si fer un test de connectivitat)

* Connectors 10A, COM, voltatge 200A

## Power supply tester
* Es un tester especialitat per a conector de components del hardware del PC

### Connectors
* Molex
* SATA
* PCI-E
* ATX (tant 20+4 com 24 pins)
* 8 pins
* 6+2 pins