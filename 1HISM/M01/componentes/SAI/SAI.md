# SAI (uninterruptile power supply o sistema d'alimentació interrumpida)
* És com un generador d'emergencia pero per servidors de PC en el cas d'apagada, la seva función es protegir el servidor tant de baixa de tenció com puja de tenció.
* La diferencia del generador comuns es que esta fet per no mantenir-se més de un temps limitat per poder guarda la informació abans de apagar-se per complet.
* Els voltatges de entrada/sortida es de 230V en els SAI.
* El SAI te dos sortides diferents, una es la salida de dispositius no critics i l'altre de dispositius critics.
* Els SAI utilit-sen rectificador, inversor DC/AC, cargador o booster DC/DC.
* En el temps que dura el SAI ences o estat fent alguna cosa fundamental o res.
* La potència del SAI s'expressa en volt-ampere (VA) o en watts (W), el VA mesura la pontecia aparent i el W la potència activa.
* Els preus del SAI son cars perqué quan millor sigui la bateria o millor resisti per dura encès.

![SAI](https://www.simpleinformatica.es/wp-content/uploads/2017/05/SAI-Sistema-de-Alimentaci%C3%B3n-Ininterrumpida.png)

# Regletes de sobretensió
* s'utilitza per protegir els aparells conectats a la regleta.
* Les limitacions d'intencitat es de 230W/A a 2500W/A
* Els preus de les regletes entre els 50 i els 100 euros en el mercat online.
![regleta](https://images-na.ssl-images-amazon.com/images/I/31r8jXAzymL._SX355_.jpg)

# CPU (unitat central de processament/Central Processing Unit)
* La seva funció és interpretar les interrupcions en el codi utilitzan operacions arimetiques-logiques i plasmarlo en la sortida.
* La CPU te dual core o multicore, vol dir, multiples nuclis per fer calculs en paraler i asi acabar les interpretació de manera més rapida i eficient. També thread que son com cores extra que esposa per si en el cas els cores tenen molts calculs dons que el thread lis ajuden a terminar-les. Els S.O tenen que tenir programes que siguin capasos de utilitzarlos.
**Es més important tenir més cores que més thread.**
* El overcloking en un programa que fa augmentes el rendiment de la CPU per aixó gasta més watt. No cal que els aplicación siguin dissenyades per utilitzar-lo, sols es utilitza en el videojocs.
* Els fabricants més popular de la venda de CPU són Inter, AMD i Qualcomm.
## Intel vs AMD
* Les dos millor empreses de CPU es troben amb proyectes diferents.
* Intel vol augmentar la velocitat dels seus relotge i menor numero de nuclis.
* AMD vol a duplica el numero de nuclis a frecuencies més que acceptables.

![intel vs AMD](https://www.avadirect.com/blog/wp-content/uploads/2015/10/intel-vs-amd.png)

# RAM (Random Access Memory/ memoria de access aleatori)
* És una memoria que es utilitza per guarda de manera tempora la informació per trobar-la més rapidament.
* És volàtil.
* Té també Dual channel, que vol dir, que se combina les dos o més memories per rendir més en l'hora de actuar. S'indica en la placa base com dual channel.
* En la actualitat les memories RAM tenen una velocitat de transmició de uns 3200 MHz, de DDR4 i amb capacitat de 16 GB 

![RAM](https://http2.mlstatic.com/memoria-ram-ddr4-8gb-3000mhz-gskill-trident-z-rgb-gaming-alto-desempeno-D_NQ_NP_681142-MLM31243235843_062019-F.jpg)