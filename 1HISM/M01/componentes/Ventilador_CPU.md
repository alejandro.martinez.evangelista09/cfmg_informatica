# Ventilació

**Respon aquestes preguntes en un document, exporta'l a PDF (o fes-lo en Markdown) i respon aquesta activitat amb el PDF o Markdown.**

* Com ha d’anar el flux de l’aire per a una correcta ventilació?

    Per baix.

* Què indica l’etiqueta d’un ventilador?

    Información dels ventilador.

* 3 formes que, normalment, indiquen la direcció de l’aire en un ventilador.

    -   Rodamiento de fluido
    -   Rodamiento de bolas
    -   Rodamiento de casquillo


* Quines parts són les que més s’escalfen en un ordinador?

    Les tarjetes grafiques, les CPU, Fonts d'alimentació y discs durs.

* Per a una bona ventilació, sobre l'entrada i la sortida d'aire. Quin ha de ser més gran o tindre més ventiladors? Com ha de ser la pressió interior de l'aire?

* Quina importància per a millorar la ventilació té la disposició dels cables?

    

* Quines conseqüències negatives pot tindre l’acumulació de pols?

    Pot donar problemes al arrencar.

* En quina unitat es mesura la velocitat del ventilador?

    rpm Radianes por minuto

* 2 factors dels ventiladors que, més o menys, fan que hi hagi més o menys flux d’aire.

    La posició d'on es posa el ventidalador y els forats per on surt l'aire.

* Com reduir el soroll causat pel ventilador sense disminuir el flux de l’aire?



* Llocs o components on pot haver un ventilador

    Pccomponetes, Amanzon , un tienda de informatica, etc ... 

* Característiques tècniques d’un ventilador. Ordena-les per importància



* De quin material estan fets els dissipadors?

    De alumini 

* De què depèn si els dissipadors porten ventilador o no
