# Actualitzacions del kernel

**Per actualitzar el nucli del linux hi ha dos formes**
* Amb
~~~
uname -r
~~~
o
* El podem trobar en el menu de grub quan fem *grub2-mkconfig*
![grub](https://maslinux.es/wp-content/uploads/2017/11/1-4.png)

* Quan descargues la actualització que es diu anaconda.

* La diferencia entre el kernel y linux es que linux es tot el S.O peró el kernel es el nucli del sistema amb les comandes i els programes de manteniment.
* I el nucli no es diferent del kernel sols que es en english.
* Te 3 numeros separats per punts.
* En el root quan fas ls hi ha la Anaconda. És un el programa que se instala en el S.O.
* Per veure la llista de actulitzacions del kerner es amb:
~~~
dnf list installed kernel
~~~
* Aquesta odre es pot fer amb usuari normal perqué no instala res ni canvi sols revisa les versions del kernel.
* Amb l'odre:
~~~
dnf update kernel
~~~
* Instalareu el kernel amb la nova versió.
* Amb l'odre:
~~~
uname -a 
~~~
* Podem veure la versió del nostra nucli.
* Quan s'instal·lin les noves versions s'afegeixen sobre la antiga. Podem triar les diferents versions amb el menu de grub.
* Podem eliminar la antiga versió per alliverar espai.
* Podem eliminarla amb l'odre:
~~~
dnf remove kernel_versio
~~~
* Posem * per aplicar a totes les versions (si es de 32 bits o de 64 bits), no cal que modifiquem el menu de grub.
* Si actualitzem el kernel de les nostres maquines no pasa res.
