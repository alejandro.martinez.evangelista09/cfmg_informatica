# Maquinas virtuales

## Questionari

* Quan parlem de host en maquines virtuals en referim els sistema operatiu i el hardware, la podem habilitar la virtualització en la configuració de el BIOS per poder utilitzar les maquines virtuales amb tota llibertat. Aunque de vegades es desabilitat perquè la pila de la bios se acaba.

* Amb la odre "sudo dnf install virtualitation" per instalar la virtualització del Fedora 27.

* Un kvm es una solució per virtualitzar maquines virtuals, podem saber que esta instalat amb l'ordre  amb el qemu ,que es un programa per emulogar maquines virtuals, podem crear S.O enter o desde 0. tambè esta el virtualbox que es una api de virtualització la diferencia entre elles es que una simula un hardware i l'altre es pensa que es una de veritat.

* L'odre qemu-system-i386 -m 2G /dev/sda crear una maquina virtual de 2G de memoria de la nostra maquina. Tenim que executar-lo en root.

* L'odre qemu-kvm -m 3G ~/Documents/ubuntu-19.10-desktop-amd64.iso es crear una maquina virtual de S.O ubuntu de 3G de memoria sense root perquè la iso esta en una carpeta del usuari.

* Amb el virt manager no podem utilitzar-lo sense ser root o ser del grup o en troba el virt-manager. amb l'odre. O estan en el grub de libvirt amb la comanda usermod -aG libvirt usuari pero tenim que sorti de la sessió i tornar entrar.

* Els discos durs ocupen molt de espai, el podem comprovar amb el l'odre dh -h /. El disc dur d'una maquina virtual es un fitxer d'image. Hi ha dos tipus de formats per guardar la image, el raw i el qcow2, el raw fa una copia exacta del disc en canvi el qcow2 es pot modificar el espai per que no ocupes tant.

* Amb l'odre qemu-img create -f qcow2 ~file.qcow2 10G no cal ser root per fer-ho. I l'odre qemu-img create -f qcow2 /var/lib/libvirt/images/disc.qcow2 15G es crea una maquina virtual sense res amb root perquè estem fen coses que requereix de permisos de super usari.

* L'odre qemu-img convert -O qcow2 ~/Disc1.img ~/Disc2.qcow2 crear 2 diferents maquines virtuals amb el format qcow2 no cal ser root perquè es crea en el nostra @HOME.

* Són pools que s'utilitzat amb molta freqüencia per exemple el  /var/lib/libvirt/images que es on se situa per defecte.

![virt-manager](https://www.virt-manager.org/wp-content/uploads/2014/01/details.png)

Amb aquets boto podem gestionar tot el hardware de la maquina.


