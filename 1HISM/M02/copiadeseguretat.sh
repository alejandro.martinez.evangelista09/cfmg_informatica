##!/bin/bash
# Nom:          copiadeseguritat.sh
# Autor:        Martinez Evangelista, Alejandro
# Data:         17/02/2020
# Descripció:   Aquet script te que comprova si tenim un copia de seguretat del directory /etc en un fitxer en /opt del nom
#               de la data de avui i en extensio tar.gz. Si existeix que informi al usuari si no que cregui un /etc en un fitxer en /opt.
# Linia d'odre: ./copiadeseguretat.sh

# Recordar que se te que executar amb sudo.
NOW=$(date +"%Y%m%d")
echo $NOW
if [ -f "/opt/$NOW.tar.gz"]
then 
    echo "ja esta fet el backup"
else
    tar czvf /opt/$NOW.tar.gz /etc
fi

echo $?
