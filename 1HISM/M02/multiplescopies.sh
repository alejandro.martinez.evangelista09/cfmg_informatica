#!/bin/bash
# Nom:          multiplescopies.sh
# Autor:        Martinez Evangelista , Alejandro
# Descripció:   Crea copies de seguretat del directoris indicats i que 
#               els desi en el directori /opt.
# Linia d'odre: ./multiplescopies.sh

for DIR in $@
do 
    mkdir -p $DIR
done
NOW=$(date +"%Y%m%d")
echo $NOW+$DIR
if [ -f "/opt/$NOW.tar.gz"]
then 
    echo "ja esta fet el backup"
else
    tar czvf /opt/$NOW.tar.gz /etc
fi
echo $?