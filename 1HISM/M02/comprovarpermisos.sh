#!/bin/bash
# Nom:          comprovarpermisos.sh
# Autor:        Martínez Evangelista, Alejandro
# Data:         17/02/2020
# Descripció:   Comprovar que el usuari guest tingui permisos, si te permisos le trellem els permisos.

if [ -rwx "/opt/20200217.tar.gz" ]:
then
    echo "Te permisos li trellem."
    chmod 0310 /opt/20200217.tar.gz
else
    echo "No te permisos."
fi
echo $?