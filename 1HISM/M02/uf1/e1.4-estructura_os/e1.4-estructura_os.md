# E1. Ejercicio 4. Estructura del sistema operativo

## Introducción

Podemos dividir el sistema operativo en las siguientes partes:
- Núcleo
- Administrador de memoria
- Administrador de entrada / salida
- Administrador del sistema de archivos

## Contenidos

Analicemos el comportamiento del sistema operativo en cada módulo. Usaremos comandos de Linux para ver qué sucede en un sistema operativo Linux.

## Entrega

### Núcleo

1. ** ¿Dónde reside el núcleo en el disco? ** Escriba el comando con la salida y diga exactamente dónde está el núcleo.

sudo cd /boot

2. ** ¿Cómo podemos mostrar la versión real del kernel que está cargada en nuestro sistema **? Por favor, muestre la salida del comando

sudo uname -a

Linux localhost.localdomain 4.18.16-300.fc29.x86_64 #1 SMP Sat Oct 20 23:24:08 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux


3. ** ¿Cómo podemos mostrar el hardware detectado realizado por el núcleo? ** Por favor, muestre la salida del comando.

lspci y lshw

4. ** ¿Cómo podemos mostrar los módulos (controladores) realmente en uso? ** ¿Qué módulo se está utilizando para la tarjeta gráfica de video?

lsmod

i937 
mi modulo se llama asus_wmi,i915.

### Administrador de memoria

1. ** ¿Cuál es la memoria 'caché' que muestra el comando 'free -m'? ** Mostrar también la salida del comando
~~~
free -m
         total        used        free      shared  buff/cache   available 
 
Mem:     3387        1551         592         158        1243        1453   
Swap:    5119           0        5119


~~~
Es la que guarda información de las aplicaciones para cuando la abras por segunda vez se abra mas rapido.

2. ** ¿Cuál es la memoria 'swap' que muestra el comando 'free -m'? **
~~~
Swap:          5119           0        5119
~~~

3. ** ¿Cómo maneja el administrador de memoria un problema de falta de memoria? **
El processo OOM Killer
Matara los procesos que este consumiendo mas RAM.

4. ** ¿Cómo podemos mostrar la 'puntuación de falta de memoria' real de una instancia de Gimp que se está ejecutando? **

htop revisas donde esta el processo del gimp 

ism4791+ 10264  0.0  0.9 975492 77112 tty2     Sl+  10:36   0:01              |   \_ gimp-2.8

ps auxf | grep gimp



cat /proc/3292/oom_score
4

### Administrador de entrada / salida

1. ** ¿Cómo podemos enumerar todas las interrupciones que conoce nuestro sistema operativo? ** Mostrar también la salida del comando
cat /proc/interrupts
~~~
           CPU0       CPU1       
  0:         10          0   IO-APIC   2-edge      timer
  5:          0          0   IO-APIC   5-edge      parport0
  8:          1          0   IO-APIC   8-edge      rtc0
  9:          0          4   IO-APIC   9-fasteoi   acpi
 16:          0         27   IO-APIC  16-fasteoi   ehci_hcd:usb1
 18:          0          0   IO-APIC  18-fasteoi   i801_smbus
 23:         29          0   IO-APIC  23-fasteoi   ehci_hcd:usb2
 24:      30523      45387   PCI-MSI 512000-edge      ahci[0000:00:1f.2]
 25:     107953      30306   PCI-MSI 327680-edge      xhci_hcd
 26:      32355      38831   PCI-MSI 1048576-edge      enp2s0
 27:      12272      50038   PCI-MSI 32768-edge      i915
 28:         13          0   PCI-MSI 360448-edge      mei_me
 29:          0        667   PCI-MSI 442368-edge      snd_hda_intel:card1
 30:        348          0   PCI-MSI 49152-edge      snd_hda_intel:card0
NMI:         51         53   Non-maskable interrupts
LOC:    2238387    3172314   Local timer interrupts
SPU:          0          0   Spurious interrupts
PMI:         51         53   Performance monitoring interrupts
IWI:        800          8   IRQ work interrupts
RTR:          0          0   APIC ICR read retries
RES:     290337     293141   Rescheduling interrupts
CAL:     397851     414497   Function call interrupts
TLB:     386724     397935   TLB shootdowns
TRM:          0          0   Thermal event interrupts
THR:          0          0   Threshold APIC interrupts
DFR:          0          0   Deferred Error APIC interrupts
MCE:          0          0   Machine check exceptions
MCP:         23         24   Machine check polls
HYP:          0          0   Hypervisor callback interrupts
HRE:          0          0   Hyper-V reenlightenment interrupts
HVS:          0          0   Hyper-V stimer0 interrupts
ERR:          0
MIS:          0
PIN:          0          0   Posted-interrupt notification event
NPI:          0          0   Nested posted-interrupt event
PIW:          0          0   Posted-interrupt wakeup event
~~~

2. ** En sistemas multiprocesador, ¿cómo distribuye el sistema operativo las interrupciones por defecto? ** ¿Cómo podemos cambiar el comportamiento predeterminado?

Distribuye entre los cores las interrupciones para poder trabajar mejor.

Con el tuned podemos cambiar el comportamiento.

3. ** ¿Cómo controla un sistema operativo un dispositivo que no tiene interrupciones? ** Explica el proceso

Pergunta a los dispositivos si ya le has mandado la información (polling) hasta que el S.O le de información de la aplicación.

4. ** ¿Qué sucederá si dos aplicaciones desean enviar datos a través del dispositivo de red al mismo tiempo? **

EL S.O organizara los datos para que vayan en orden.

### Administrador del sistema de archivos

1. ** ¿Cuál es la estructura típica de la carpeta de Linux? ** Describa qué es y el uso de cada carpeta.
~~~
bin: Es un directorio donde se guarda los programas ejecutables.

boot:Es un directorio estatico donde guarda los programas de arranque.

dev:Contiene todos los archivos de los dispositivos.

etc:Contiene script que se ejecutan cuando arrancas o cuando cambias de nivell de 
ejecución.

home: Es el directorio donde se guarda los usuarios y todos los ficheros.

lib: Es un directorio que contiene los ficheros que utiliza el sistema en la biblioteca. 

lib64:Guarda los ficheros que estan a 64 bits.

lost+found:Es un contrutor que utiliza fsck para repara el S.O

media:Representa el punto de montaje de todos los volúmenes lógicos que se montan temporalmente.

mnt:Es lo mismo que el directorio media solo que se montan permanentement.

opt:Es una carpeta vacía donde por cualquier cosa.

proc:Es donde estan los procesos.

root:Es donde se guarda las actividades en superusuario.

run:Es un directorio dedicado a guardar i ejecutan programas de arranque 

sbin:contiene binarios reservados para los administradores del sistema.

srv:Es para guardar ficheros cuandor el S.O sea un servidor.

sys:Muestra información relacionada con subsistemas del núcleo, dispositivos de hardware y controladores de dispositivos asociados.

tmp:Es un directorio donde se guardan ficheros temporales.

usr:Es donde reside las aplicacones en codigo orientadas al usuario.

var:Es un directorio que se guarda de todo.
~~~



2. ** ¿Cómo podemos:? **
- Mueva un archivo que reside en /usr/local/src/file.md a la carpeta / opt: `mv /usr/local/src/file.md /opt`
- Copie un archivo que reside en /usr/local/src/file.md en la carpeta / opt: `cp /usr/local/src/file.md /opt`
- Mueva un archivo en /usr/file.md a / usr / local si actualmente estamos en la ruta / usr / local: `mv ../file.md ./`
- Cree el archivo .gitignore con el comando 'touch' y luego intente enumerarlo (ls). ¿Lo que pasa? `touch -gitignore`    
`El archivo se vuelve invisible hasta que se pueda ver con el comando ls -lisa `

