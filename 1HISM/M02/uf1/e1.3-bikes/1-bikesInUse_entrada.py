#!/usr/bin/python3
# coding=utf-8

########################################################################
## AUTOR: 		Josep Maria Viñolas Auquer			      			  ##
## VERSIO: 		0.2													  ##
## LLICENCIA: 	AGPLv3                                                ##
## DESCRIPCIO:  Es conectarà a la api json de bicing i mostrarà les   ##
##              consola											      ##
## REQUISITS:   sudo pip3 install requests					          ##
## EXECUCIO:	python3 1-bikesInUse_entrada.py                       ##
########################################################################

# Aquesta llibreria ens permet fer peticions a una URL.
# En el nostre cas l'usarem per conectar a les dades de la API.
import requests

# Guardarem totes les dades que ens retorna el web en una variable
# anomenada dataapi. El sistema operatiu serà l'encarregat de trobar
# un lloc a la memòria RAM de l'ordinador on desar-les
dataapi=requests.get("https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status").json()
# Mostrem per la consola les dades emmagatzemades a la variable dataapi
print(dataapi)
