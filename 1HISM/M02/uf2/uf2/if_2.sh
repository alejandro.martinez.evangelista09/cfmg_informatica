#!/bin/bash
# Name:     if_2.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020

CADENA1="un"
CADENA2="dos"
CADENA3=""

if [ $CADENA1 = $CADENA2 ]; then
    echo "\$CADENA1 és igual a \$CADENA2"
#  Comprueva que el valor $CADENA1 es igual que $CADENA2.
elif [ $CADENA1 != $CADENA2 ]; then
    echo "\$CADENA1 no és igual a \$CADENA2"
#  Comprueva que el valor $CADENA1 es diferente que el $CADENA2.
fi

if [ -z $CADENA3 ]; then
    echo "\$CADENA3 està buida"
#  Comprueva que el valor $CADENA3 si no tiene nada.
fi