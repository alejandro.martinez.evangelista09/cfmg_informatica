#!/bin/bash
# Name:     if_2.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020

NUM=0

until [ $NUM -gt 10 ]; do
# El until se utiliza como el while solo que no finaliza cuando el valor se true
# si no cuando el valor se false.
    echo "\$NUM: $NUM"
    let NUM=$NUM+1
done