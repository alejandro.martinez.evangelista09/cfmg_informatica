#!/bin/bash
# Codi d'odre:      bash test_fitxer.sh
# Nom:              Martinez Evangelista, Alejandro
# Descripció:       Tenim que posar la data i l'hora de avui en el noupload.txt, 
#                   si ja existeix dons tenim que comprobar que existeix amb el nom
#                   de upload.txt.
FECHA=$(date -Idate)
HORA=$(date +%H:%M:%S)
if [ -e /tmp/upload.txt ];then 

    echo "$FECHA and $HORA" >> /tmp/upload.txt
else
    echo "$FECHA and $HORA" >> /tmp/noupload.txt
fi