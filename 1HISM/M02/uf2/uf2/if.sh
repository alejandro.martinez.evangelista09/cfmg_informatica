#!/bin/bash
# Name:     if.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020
DIRECTORI="/tmp/test"

ORDRE="/bin/mkdir $DIRECTORI"
# Crea un directorio en /bin que el nombre de /tmp/test.
if $ORDRE
    then
    echo "$DIRECTORI s'ha creat"
else
    echo "$DIRECTORI no s'ha pogut crear"
fi