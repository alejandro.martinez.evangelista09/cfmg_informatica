#!/bin/bash
# Name:     if_2.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020

select OPCIO in opcio_1 opcio_2 opcio_3
# Con el select muestra opciones para elegir.
  do
  if [ $OPCIO ]; then
      echo "Opció escollida: $OPCIO"
      break
      # El break se utiliza para cortar linias.
  else
      echo "Opció no vàlida"
  fi
done