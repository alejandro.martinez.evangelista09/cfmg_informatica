#!/bin/bash
# Filename:         2-encadena.sh
# Autor:            Martínez Evangelista, Alejandro (ism47916106)
# Date:             27/01/2020
# Version:          0.1
# License:          This is free software, licensed under the GNU General Public License v3.
#                   See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:            ./2-encadena.sh
# Description:      El script demanara tres fitxers com arguments, el primer fitxer es el de sortidad on posara el contingut dels dos fitxers 
cat $2 $3 > $1
