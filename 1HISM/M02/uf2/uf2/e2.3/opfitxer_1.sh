#!/bin/bash
# Name:         if_2.sh
# Author:       Martinez Evangelista, Alejandro
# Date:         10/02/2020
# Descripció:   Comprova si existeix el fitxer, si no existeix dons crea un.

FITXER = "/tmp/prova"

if [ -f $FITXER ];
then 
    echo "Existeix"
else
    touch "/tmp/prova"
    # cat ""/tmp/prova
    echo "ya esta creado porque no existia"
fi