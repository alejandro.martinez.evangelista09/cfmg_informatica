#!/bin/bash
# Filename:         3-encadena-varible.sh
# Autor:            Martínez Evangelista, Alejandro (ism47916106)
# Date:             27/01/2020
# Version:          0.1
# License:          This is free software, licensed under the GNU General Public License v3.
#                   See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:            ./3-encadena-variable.sh
# Descrption:       Aquets script fa el mateix que el segon sols canvia que els fitxer concatenar sigui varibles
rm sortida.txt
for FILE in $@
    do
    cat $@ > sortida.txt
    done