#!/bin/bash
# Filename:         7-usuaris.sh
# Autor:            Martínez Evangelista, Alejandro (ism47916106)
# Date:             27/01/2020
# Version:          0.1
# License:          This is free software, licensed under the GNU General Public License v3.
#                   See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:            ./7-usuaris.sh
# Descrption:       Fem un script que digui quants usuaris hi ha conectats al sistema (who).
who | wc -l