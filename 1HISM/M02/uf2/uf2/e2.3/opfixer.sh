#!/bin/bash
# Name:     if_2.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020


#  Feu un script "1-opfitxers.sh" que comprovi si existeix el fitxer "/tmp/prova". 
#  Si no existeix el crea. Feu que informi a l'usuari del que ha passat.

#    Feu un script "2-opfitxers.sh" que comprovi si el fitxer ""/tmp/prova" existeix i no està buit.
#    Si està buit feu que s'hi afegeixi el text "Dades fitxer" dins. Feu que informi a l'usuari del que ha passat.

#  Feu un script "3-opfitxers.sh" que comprovi si tenim permisos d'execució sobre el fitxer "/tmp/prova". 
#  Si no tenim permisos d'execució assigneu-los per al nostre usuari. Feu que informi a l'usuari del que ha passat.

# Feu un script "4-opfitxers.sh" que ens digui quin dels dos fitxers que li passem com a paràmetres és el més recent en data.
# És a dir, el cridarem amb "4-opfitxers.sh fitxer1.txt fitxer2.txt" i ens retornarà alguna cosa similar a 
# "El fitxer fitxer1.txt és més recent que el fitxer2.txt"
