#!/bin/bash
# Filename:         5-tamany.sh
# Autor:            Martínez Evangelista, Alejandro (ism47916106)
# Date:             27/01/2020
# Version:          0.1
# License:          This is free software, licensed under the GNU General Public License v3.
#                   See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:            ./5-tamany.sh
# Descrption:       Tenim que fer un script que rebrà com argument un nomble variable de noms de fitxers i el mostra per pantalla el tamany i el nom.
for FILE in $@
do 
    du -bsh $FILE
    done
