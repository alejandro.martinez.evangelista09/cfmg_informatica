#!/bin/bash
# Name:         if_2.sh
# Author:       Martinez Evangelista, Alejandro
# Date:         10/02/2020
# Descripció:   Comprova si qual dels dos fitxer es el més recent.

#if [ ! -f "$1" ]
#then
#    echo "No existeix $1!!"
#    exit 
#fi

#if [ ! -f "$2" ]
#then
#    echo "No existeix $2!!"
#    exit # Para que salga del script
#fi

if [ ! -f "$1"] || [ ! -f "$2"] # || = or i && = and
then
    echo "has de passar dos fitxers com arguments!!"
    exit
fi

if  [ $1 -nt $2 ]
 then
    echo "El {$1} és més recent que el {$2}"
else
    echo "El {$2} és més recente que el {$1}"
    fi 