#!/bin/bash
# Filename:         4-encadena_bandera.sh
# Autor:            Martínez Evangelista, Alejandro (ism47916106)
# Date:             29/01/2020
# Version:          0.1
# License:          This is free software, licensed under the GNU General Public License v3.
#                   See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:            ./4-encadena_bandera.sh
# Descrption:       Tenim que fer el mateix que a vas pero ficant dins de flags i afeguin dos fitxers


while test $# -gt 0; do 
            case "$1" in 
            -i1)
                shift
                INPUT1=$1
                shift;;
            -i2)
                shift
                INPUT2=$1
                shift;;
            -o)
                shift
                OUT=$1
                shift;;
            *)
                echo "$0:Hay un error de:$1"
                shift;;

            esac
done
cat $INPUT1 $INPUT2 > $OUT
