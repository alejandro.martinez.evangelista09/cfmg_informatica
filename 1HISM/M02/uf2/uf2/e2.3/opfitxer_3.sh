#!/bin/bash
# Name:         if_2.sh
# Author:       Martinez Evangelista, Alejandro
# Date:         10/02/2020
# Descripció:   Comprova que tingui permisos de execucio en /tmp/prova.

FITXER = "/tmp/prova" 

if [ -x $FITXER ] then
    echo "Tens permisos de execucio"
    else
    chmod 0754 /tmp/prova
    #chown u+x /tmp/prova
    echo "Abans no teniu premisos de execució, pero ara si."
    fi 