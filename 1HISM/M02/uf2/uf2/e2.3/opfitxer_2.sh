#!/bin/bash
# Name:         if_2.sh
# Author:       Martinez Evangelista, Alejandro
# Date:         10/02/2020
# Descripció:   Comprova si esta el fitxer i si no esta buit.

FITXER= "/tmp/prova"

if [ -s $FITXER];
then 
    echo "Existeix"
else
    touch "/tmp/prova"
    echo "Dades fitxer" > $FITXER
    echo "Ya esta creat"
fi