#!/bin/bash
#   autor:         Alejandro, Martinez Evangelista
#   Nom:           crea_dir.sh
#   Descripcio:    Creador de directoris
for DIR in $@
do 
    mkdir -p $DIR
done
tree