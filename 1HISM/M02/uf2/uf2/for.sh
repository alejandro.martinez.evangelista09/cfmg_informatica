#!/bin/bash
# Name:     if_2.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020
for HOST in www.google.com www.altavista.com www.yahoo.com
# Fa un bucle per revisar si la addresa ip dels dns lookup.
do
  echo "-----------------------"
  echo $HOST
  echo "-----------------------"
  
  /usr/bin/host $HOST
  echo "-----------------------"

done