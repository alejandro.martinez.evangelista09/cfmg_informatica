#!/bin/bash
# Name:     if_2.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020

NUM=0

while [ $NUM -le 10 ]; do
#  Crea un bucle hasta que que el numero se mas grande o igual que 10.
    echo "\$NUM: $NUM"
    let NUM=$NUM+1
    # let serveix per fer operacions sense por la operació dins
    # de dobles parentecis.
done