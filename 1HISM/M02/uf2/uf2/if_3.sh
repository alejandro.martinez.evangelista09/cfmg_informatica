#!/bin/bash
# Name:     if_2.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020
let NUM1=1
let NUM2=2
let NUM3=3

if [ $NUM1 -ne $NUM2 ] && [ $NUM1 -ne $NUM3 ]; then
    echo "\$NUM1 és diferent a \$NUM2 y \$NUM3"
#  Comprueva que el valor $NUM1 no sea igual que els valors $NUM2 i $NUM3.
fi

if [ $NUM1 -lt $NUM3 ]; then
    echo "\$NUM1 és menor que \$NUM3"
#  Comprueva que el $NUM1 sigui menor que $NUM3.
fi