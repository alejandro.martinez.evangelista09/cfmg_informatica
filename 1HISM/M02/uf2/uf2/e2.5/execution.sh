#!/bin/bash
#autor:         Alejandro, Martinez Evangelista
#Nom:           execution.sh
#Descripcio:    Fer un codi que doni un error que no sigui 0,1.255,128,128 + n,
#               126,127,130,255 \* i 2.
# echo -e ls -- option
# echo -e "====================="
#ls --option 2> /mnt/sortidaerr.out
# Incorrect usage, so exit status will be 2.
#echo "Exit status" $? 
/home/sudinves/www/gestion/app/wkhtmltopdf --lowquality '/tmp/knp_snappy54fb768a2073c2.63614363.html' '/tmp/knp_snappy54fb768a207716.27493945.pdf'.