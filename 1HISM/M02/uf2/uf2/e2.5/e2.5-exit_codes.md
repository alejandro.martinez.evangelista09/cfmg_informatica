# E2. Exercici 5. Codis de sortida

## Introducció

Un codi de sortida, o de vegades conegut com a codi de retorn, és el codi retornat a un procés per part d'un executable. En els sistemes POSIX, el codi de sortida estàndard és 0 per a l'èxit i qualsevol número d'1 a 255 per a qualsevol altra cosa.

## Continguts

Aquests són els codis d'error estàndard a Linux o UNIX.

- 1 - Per a errors generals
- 2 - Utilització indeguda de shell builtins (segons la documentació de Bash)
- 126 - L'ordre invocat no es pot executar
- 127 - "ordre no trobada"
- 128 - Argument invàlid per sortir
- 128 + n - Senyal d'error fatal "n"
- 130 - Guió acabat amb Control-C
- 255 \ * - Sortida de l'estat fora de rang

## Entrega

1. **Expliqueu què farà el següent script i quins en són els possibles codis de sortida. Analizeu-lo abans d'executar-lo.**

~~~bash
#!/bin/bash
# Check the exit status

cd $some_directory
if [ "$?" = "0" ]; then
	rm *
else
	echo "Cannot change directory!" 1>&2
	exit 1
fi                                
~~~

​	Explicació: Vol accedir a la carpeta que conte la variable some_directory. si entra sense error borrar tot el que conte aquella carpeta, sino et diu que dona error. Possibles codis de sortida 0,1,127,126

2. **Expliqueu què farà el següent script i quins en són els possibles codis de sortida. Executeu-lo i poseu una crida exemple que doni com a sortida error i una altra exitosa.**

~~~bash
#!/bin/bash
if [ "$1" = "foo" ]; then
  exit 1
fi
exit 0
~~~

​	Ordre amb sortida errònia: sh sortida.sh 
#1/script_prova.sh: línea 2: EOF inesperado mientras se buscaba un `"' coincidente
#1/script_prova.sh: línea 6: error sintáctico: no se esperaba el final del fichero
​	Ordre amb sortida correcta:sh sortida.sh

3. **Quin codi d'error obtindrem d'un intent de conexió a una màquina remota inexistent amb ssh? Poseu exemple**. 
~~~bash
[ism47916106@a17 uf2]$ ssh root@b10
ssh: Could not resolve hostname b10: Name or service not known
[ism47916106@a17 uf2]$ echo $?
255
~~~

4. **Feu un script que mostri amb echo la comanda que s'executarà, després executi la comanda i finalment mostri el codi d'error per a diferentes comandes que donin diferents codis d'error. Busqueu a Internet alguna ordre que proporcioni algun codi d'error que encara no hagi sortit.**

~~~bash

~~~