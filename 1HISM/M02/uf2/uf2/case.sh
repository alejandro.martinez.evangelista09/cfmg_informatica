#!/bin/bash
# Name:     if_2.sh
# Author:   Martinez Evangelista, Alejandro
# Date:     10/02/2020

for NUM in 0 1 2 3
do
  case $NUM in
  # El case se pone dentro de un for.
      0)
	  echo "\$NUM és igual a zero";;
      1)
	  echo "\$NUM és igual a un";;
      2)
	  echo "\$NUM és igual a dos";;
      3)
	  echo "\$NUM és igual a tres";;
  esac
done