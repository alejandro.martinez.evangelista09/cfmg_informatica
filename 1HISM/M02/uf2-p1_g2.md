# UF2. Prova 1.

## Entrega

En finalitzar l'exercici s'enviarà per a la correcció:
- Aquesta memòria de la prova amb les preguntes 5 i 6 respostes
- Els fitxers de codi (scripts) realitzats:
  - exercici1.sh
  - exercici2.sh
  - exercici3.sh
  - exercici4.sh
  - prova.sh

## Enunciat

Creeu una carpeta amb el vostre cognom i nom separada per guions baixos. Per exemple, vinolas_josep_maria. Dins d'aquesta carpeta realitzeu els exercicis següents.

1. [1 punt] **Feu un script anomenat exercici1.sh que crei un directori per cada paràmetre que se li passi. Una vegada creat executeu-lo amb la següent ordre: **

   ```./exercici1.sh da db dc da/sd1 da/sd2 db/sd1```

   **El resultat haurà de ser una estructura de directoris com aquesta:**

   ```
   cognom_nom
   ├── da
   │   ├── sd1
   │   └── sd2
   ├── db
   │   └── sd1
   ├── dc
   └── exercici1.sh
   ```

2. [1 punt] **Feu un script anomenat exercici2.sh que faci una búsqueda dels directoris que contenen d1 al seu nom i desi el resultat en un fitxer a db/exercici2.out.**

3. [2 punt] **Feu un script anomenat dc/exercici3.sh que prengui com a opcions a i b que seran els noms dels fitxers a crear als directoris sda i sdb existents. Una vegada creat executeu-lo amb la següent ordre:**

   ```./exercici3.sh -a exercici3.out -b exercici3.bak```

4. [2 punt] **Feu un script anomenat exercici4.sh que crei el fitxer dc/exercici4.txt si existeix el fitxer da/sd1/exercici3.out. En cas contrari (si no existeix el fitxer) crei el fitxer dc/exercici4.fail**

5. [1 punt] **Feu una còpia del directori da dins del directori dc/20190228 fent servir l'eina de còpies de seguretat rdiff-backup. Si cal l'haureu d'instal.lar i buscar-ne com s'usa. Heu de substituïr 20190228 a la vostra ordre per la data generada automàticament a partir de l'hora del sistema operatiu.**

   - Ordre complerta per realitzar la còpia:

6. [1 punt] **Feu una còpia de seguretat de tot l'arbre de directoris dins de cognom_nom/da que heu creat amb fitxers inclosos. Feu-la utilitzant l'ordre tar amb compressió. El fitxer ha de dur per nom la data i hora actuals i tenir d'extensió tar.gz i desar-se a la carpeta cognom_nom/dc.**

   - Ordre complerta per crear el fitxer:

7. [2 punt] **Creeu ara un script anomenat prova.sh que cridi a tots els exercicis seguits. És a dir, aquest script haurà de contenir una capçalera amb les dades habituals que el descriuen i tot seguit una sortida de pantalla on s'indicarà l'exercici (echo) i el codi per executar l'script corresponent. Per exemple:**

```
#!/bin/bash
#
# (línies de capçalera)
#

echo "Exercici 1"

(ordre executar exercici 1)

echo "Exercici 2"

(ordre exercutar exercici 2)

...
```




