# Exercicis UF2 preparació prova

Creeu una carpeta amb el vostre cognom i nom separada per guions baixos. Per exemple, vinolas_josep_maria. Dins d'aquesta carpeta realitzeu els exercicis següents.

1. [2 punt] **Feu un script anomenat avuiquindiaes.sh que retorni la data actual en format 20200217-0840. Ha de mostrar per pantalla: Avui és 20200217**

2. [2 punt] **Feu un script que comprovi si tenim feta la còpia de seguretat del directori /etc en un fitxer al directori /opt de nom la data d'avui i extensió tar.gz. Si el fitxer existeix feu que informi a l'usuari. En cas que no existeixi feu que es faci la còpia de seguretat del directori /etc en aquest fitxer a /opt amb nom la data d'avui i extensió .tar.gz**

3. [2 punts] **Comproveu ara si l'usuari guest té permisos d'alguna mena sobre aquest fitxer. Si en té, feu que aquest usuari (guest) perdi tot tipus de permisos sobre el fitxer. Si no en té només cal que informeu a l'usuari que l'usuari no hi té permisos.**

4. [1 punt] **Feu un script que faci còpies de seguretat dels directoris indicats que se li passaran com a arguments i en desi un fitxer per cadascun al directori /opt. Els noms dels fitxers han de ser, per exemple si executem la següent ordre:**

   ```./backup.sh /etc/sysctl.d /etc/ppp /etc/openvpn```
   
   Ens generi els següents fitxers
   
   - /opt/20200217-sysctl.d.tar.gz
   - /opt/20200217-ppp.tar.gz
   - /opt/openvpn.tar.gz

[1 punt] **Si feu que no s'executi si no existeix algun dels directoris que se li passen i ho indica a l'usuari: El directori /etc/... no existeix"**

5. [2 punt] **Feu ara un script que se li passin els arguments amb banderes (flags) de la següent manera:**

   ```./backup.sh -d /opt/20200217.tar.gz -o /etc```