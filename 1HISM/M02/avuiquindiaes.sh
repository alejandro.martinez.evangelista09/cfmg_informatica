#!/bin/bash
# Nom:          avuiquindiaes.sh
# Autor:        Martinez Evangelista, Alejandro
# Data:         17/02/2020
# Descripció:   Comprova quin dia es avui y que mosti en pantall així "20200210"
# Linia d'odres:./avuiquindiaes.sh

echo "Hola avui es:"
echo $(date +%Y%m%d)
