# Comandos

## git 
* Clonar git 
```
git clone https://gitlab.com/cfmg-smx/m02.git
```
* Cambiar de directorio
```
cd m02 (Para entrar en el directorio)
```
* Actualizar el repositorio clonado
```
git pull (actualizar el repositorio que esta en tu PC)
```
* Volver el home (directorio)
```
cd ..
```
## Typoda 
* Descargar typoda en la pagina web
https://typora.io/#linux

* instalarlo
ir
```
cd Typoda_linux-x64
```
* Ejecutarlo
```
./Typora
```
* Si da error tienes que instalar el paquete libXss.so.1
```
sudo dnf install libXScvnSaver-devel
```

* Instalar virt-viewer
```
sudo dnf install virt-viewer
sudo apt install virt-viewer y
Windows: https://virt-manager.org/download
```

## vi

sirve para editar un fichero en codigo.
* inserta
```
La tecla "i" para insertar letras
```
* Salir de insertar
~~~
La tecla esc
~~~
* poner en los numeros de las linias
~~~
:set number
~~~
## Tar

* archivar (comprimir) los archivos
~~~
tar cf arxivar.tar exercici/
~~~
* desarchivar los archivos
~~~
tar xf arxivar.tar
~~~
* archivar y comprimir
~~~
tar czfv arxivat.tar.gz
~~~
* descomprimir y externe
~~~
tar xfv arxivar.tar.gz
~~~
