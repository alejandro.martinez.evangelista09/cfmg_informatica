#permisos de root adelantados
sudo echo

# Descarga y descomprimir del tema
wget -O fallout-grub-theme.zip https://github.com/shvchk/fallout-grub-theme/archive/master.zip
unzip fallout-grub-theme.zip

#Inicio de la instalacion en español
sed -i -r -e '/^\s+# EN$/{n;s/^(\s*)/\1# /}' -e '/^\s+# '"ES"'$/{n;s/^(\s*)#\s*/\1/}' fallout-grub-theme-master/theme.txt

#creacion de los archivos para el tema
sudo mkdir -p /boot/grub2/themes/fallout-grub-theme

#Copia de los archivos
sudo cp -r fallout-grub-theme-master/* /boot/grub2/themes/fallout-grub-theme

#Quitando otros temas antes de continuar
sudo sed -i '/^GRUB_THEME=/d' /etc/default/grub

#Revisando que el grub tiene salida grafica
sudo sed -i 's/^\(GRUB_TERMINAL\w*=.*\)/#\1/' /etc/default/grub

#Quitando lineas en blanco
sudo sed -i -e :a -e '/^\n*$/{$d;N;};/\n$/ba' /etc/default/grub

#Añadiendo linea al final para separar el tema
echo | sudo tee -a /etc/default/grub

#Añadiendo el tema al grub
echo "GRUB_THEME=/boot/grub2/themes/fallout-grub-theme/theme.txt" | sudo tee -a /etc/default/grub

#Eliminando archivos temp
rm -rf fallout-grub-theme.zip -rd fallout-grub-theme-master

#Creando la configuracion adecuada
sudo grub2-mkconfig -o /boot/grub2/grub.cfg

